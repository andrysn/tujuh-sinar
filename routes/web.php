<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('', 'AntrianController@indexAntrian')->name('antrian');
Route::get('antrian/konfirmasi/{id}', 'AntrianController@konfirmasi')->name('antrian.konfirmasi');
Route::get('antrian/cetak', 'AntrianController@AntrianCetak')->name('antrian.cetak');
Route::get('antrian/desain', 'AntrianController@AntrianDesain')->name('antrian.desain');

Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function() {
    Route::get('tes', function() {
    	return Hash::make('admin');
	});

	Route::get('/', 'Dashboard@cek');
	Route::get('/dashboard', 'Dashboard@index')->name('dash');

	Route::group(['prefix' => 'keuangan'], function() {
		Route::get('/utang', 'BeliController@utang')->name('utang');
		Route::get('/utang/{id}', 'BeliController@utangbeli')->name('utangbeli');
		Route::resource('/piutang', 'PiutangController');
		Route::resource('/pembelian', 'BeliController');
		Route::resource('/belidet', 'BeliController');
		Route::resource('/tmpbeli', 'TmpBeliController');
		Route::get('harga/{id}', 'TmpBeliController@show');
	});


	Route::group(['prefix' => 'laporan'], function() {
		Route::get('/pembelian', 'BeliController@laporan')->name('lapbeli');
	
		Route::group(['prefix' => 'cetak'], function() {
			Route::get('/detailbeli/{id}', 'BeliController@cetakdetail')->name('cetdetbeli');
			Route::get('/beli/{awal}&{akhir}', 'BeliController@cetak')->name('cetbeli');		
		});
	});


	Route::group(['prefix' => 'master'], function() {

		Route::resource('/tools', 'ToolsController');
		Route::group(['prefix' => 'tools'], function() {
			Route::resource('/barang', 'BarangController');
			Route::resource('/printer', 'PrinterController');
			Route::resource('/potong', 'JenisPotongController');
			Route::resource('/display', 'JenisDisplayController');
		});
	

		Route::resource('/harga', 'HargaController');
		Route::resource('/member', 'MemberController');
		Route::resource('/supplier', 'SupplierController');
		Route::resource('/costumer', 'PelangganController');
		Route::post('/costumer/storeModal', 'PelangganController@storeModal')->name('costumer.storeModal');
		Route::post('harga/merchant/', 'HargaController@merchant')->name('harga.merchant');
		Route::post('harga/outdoor/', 'HargaController@outdoor')->name('harga.outdoor');
		Route::post('harga/print-quarto/', 'HargaController@printQuarto')->name('harga.printQuarto');
		Route::post('harga/indoor/', 'HargaController@indoor')->name('harga.indoor');
		Route::post('harga/large-format/', 'HargaController@largeFormat')->name('harga.largeFormat');
		Route::post('harga/costum-produk/', 'HargaController@costumProduk')->name('harga.costumProduk');
		Route::post('harga/merchant/update/', 'HargaController@merchantEdit')->name('harga.merchantUpdate');
		Route::post('harga/outdoor/update/', 'HargaController@outdoorEdit')->name('harga.outdoorUpdate');
		Route::delete('harga/merchant/destroy/{id}', 'HargaController@merchantDestroy')->name('harga.merchantDestroy');
		Route::post('harga/large-format/update/', 'HargaController@largeFormatEdit')->name('harga.largeFormatUpdate');
		Route::post('harga/print-quarto/update/', 'HargaController@printQuartoEdit')->name('harga.printQuartoUpdate');
		Route::post('harga/indoor/update/', 'HargaController@indoorEdit')->name('harga.indoorUpdate');
		Route::post('harga/costum-produk/update/', 'HargaController@costumProdukEdit')->name('harga.costumProdukUpdate');
		Route::delete('harga/outdoor/destroy/{id}', 'HargaController@outdoorDestroy')->name('harga.outdoorDestroy');
		Route::delete('harga/large-format/destroy/{id}', 'HargaController@largeFormatDestroy')->name('harga.largeFormatDestroy');
		Route::delete('harga/indoor/destroy/{id}', 'HargaController@indoorDestroy')->name('harga.indoorDestroy');
		Route::delete('harga/print-quarto/destroy/{id}', 'HargaController@printQuartoDestroy')->name('harga.printQuartoDestroy');
		Route::delete('harga/costum-produk/destroy/{id}', 'HargaController@costumProdukDestroy')->name('harga.costumProdukDestroy');
	});

	Route::group(['prefix' => 'transaksi'], function() {
		Route::resource('antrian', 'AntrianController');
		Route::resource('order', 'OrderKerjaController');
		Route::resource('produksi', 'ProduksiController');
		Route::resource('gudang', 'GudangController');
		
		Route::group(['prefix' => 'order'], function() {

			Route::get('update_status/{id}', 'OrderKerjaController@update_status')->name('order.update_status');

			Route::group(['prefix' => 'outdoor'], function() {
				Route::get('data/{barang}/{editor}/{pelanggan}/{qty}/{p}/{l}', 'HargaOutdoorController@getData');
			});

			Route::group(['prefix' => 'indoor'], function() {
				Route::get('data/{barang}/{editor}/{pelanggan}/{qty}/{p}/{l}/{tipe}', 'HargaIndoorController@getData');
				
			});

			Route::group(['prefix' => 'merchant'], function() {
				Route::get('data/{barang}/{pelanggan}/{qty}', 'HargamerchantController@getData');
				
			});

			Route::group(['prefix' => 'print'], function() {
				Route::get('data/{barang}/{pelanggan}/{qty}/{ukuran}/{editor}/{tipe_print}/{kaki}', 'HargaPrintController@getData');
				
			});

			Route::group(['prefix' => 'costum'], function() {
				Route::get('data/{nama_produk}/{produk}/{pelanggan}/{qty}', 'HargaCostumController@getData');
				
			});

			Route::post('tambah/{id}', 'OrderKerjaController@detail')->name('order.detail');
			Route::delete('hapus/{id}', 'OrderKerjaController@destroySubKerja')->name('order.subkerja.hapus');

			Route::group(['prefix' => 'store'], function() {
				Route::post('outdoor/', 'OrderKerjaController@storeOutdoor')->name('storeOutdoor');
				Route::post('indoor/', 'OrderKerjaController@storeIndoor')->name('storeIndoor');
				Route::post('merchant/', 'OrderKerjaController@storemerchant')->name('storeMerchant');
				Route::post('print-quarto/', 'OrderKerjaController@storePrintQuarto')->name('storePrintQuarto');
				Route::post('costum-produk/', 'OrderKerjaController@storeCostumProduk')->name('storeCostumProduk');
			});
		});

		Route::group(['prefix' => 'produksi'], function() {
			Route::get('show/{id}', 'ProduksiController@detail')->name('produksi.detail');
			Route::get('update_status/{id}', 'ProduksiController@update_status')->name('produksi.update_status');
		});

		Route::group(['prefix' => 'gudang'], function() {
			Route::get('update_status/{id}', 'GudangController@update_status')->name('gudang.update_status');
		});
		
	});

	Route::resource('/editor', 'EditorController');
	Route::resource('/pengguna', 'PenggunaController');
	Route::get('/pengguna/pass/{id}', 'PenggunaController@ganti')->name('gantiPass');


	Route::group(['prefix' => 'inventory'], function() {
		Route::get('pembelian/barang/{id}', 'PembelianController@getBarang');
		Route::resource('stokbarang', 'StokBarangController');
	});

});

Auth::routes();