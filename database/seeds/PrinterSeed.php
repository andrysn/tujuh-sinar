<?php

use Illuminate\Database\Seeder;

class PrinterSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('printers')->delete();

    	$data = array(
    		array(
                'kd_printer' => 'C-8001',
                'nm_printer' => 'Xerox D-95 B/W',
                'merk' => 'Xerox',
                'status' => 'ada',
                'keterangan' => 'ini keterangan printer',
                ),
            array(
    			'kd_printer' => 'D-0012',
    			'nm_printer' => 'Xerox D-10 Full',
    			'merk' => 'Xerox',
    			'status' => 'ada',
    			'keterangan' => 'ini keterangan printer',
    			),
    		);

    	DB::table('printers')->insert($data);
    }
}
