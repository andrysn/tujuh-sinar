<?php

use Illuminate\Database\Seeder;

class HargaOutdoorSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('harga_outdoors')->delete();


    	DB::table('harga_outdoors')->insert($data);
    }
}
