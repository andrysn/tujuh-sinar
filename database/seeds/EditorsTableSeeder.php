<?php

use Illuminate\Database\Seeder;

class EditorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('editors')->delete();

        $data = array(
            array(
                'produk_id' => 1,
                'nama_finishing' => 'Mata ayam'
    			),
            array(
                'produk_id' => 1,
                'nama_finishing' => 'Lebih putih'
    			),
            array(
                'produk_id' => 1,
                'nama_finishing' => 'Non Mata ayam'
    			),
            array(
                'produk_id' => 2,
                'nama_finishing' => 'Laminasi Doft'
    			),
            array(
                'produk_id' => 2,
                'nama_finishing' => 'Laminasi Glossy'
    			),
            array(
                'produk_id' => 2,
                'nama_finishing' => 'Mata ayam'
    			),
            array(
                'produk_id' => 2,
                'nama_finishing' => 'Non Finishing'
    			),
            array(
                'produk_id' => 4,
                'nama_finishing' => 'Laminasi Doft'
    			),
            array(
                'produk_id' => 4,
                'nama_finishing' => 'Laminasi Glossy'
    			),
            array(
                'produk_id' => 4,
                'nama_finishing' => 'Cutting'
    			),
            array(
                'produk_id' => 4,
                'nama_finishing' => 'Laminasi + Cut Doft'
    			),
            array(
                'produk_id' => 4,
                'nama_finishing' => 'Laminasi + Cut Glossy'
    			),
            array(
                'produk_id' => 4,
                'nama_finishing' => 'Non Finishing'
    			)
    		);

    	DB::table('editors')->insert($data);
    }
}
