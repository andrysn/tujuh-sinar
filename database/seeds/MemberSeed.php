<?php

use Illuminate\Database\Seeder;

class MemberSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('members')->delete();

    	$data = array(
    		array(
    			'nm_tipe' => 'Umum',
    			'keterangan' => 'Member untuk umum',
    			),
    		array(
    			'nm_tipe' => 'Perusahaan',
    			'keterangan' => 'Member untuk perusahaan',
				),
			array(
    			'nm_tipe' => 'Komunitas',
    			'keterangan' => 'Member untuk komunitas',
    			),
    		);

    	DB::table('members')->insert($data);
    }
}
