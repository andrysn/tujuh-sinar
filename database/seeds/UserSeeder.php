<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user')->delete();

    	$data = array(
    		array(
    			'nama' => 'admin.id', 
    			'alamat' => 'Lambung Mangkurat',
    			'no_telp' => '0836367333',
    			'username' => 'admin',
    			'password' => '$2y$10$8SZTOXjZjIIwqDB4L/Q.kOiERrheHctF809U2AP/4Cd0mk6ySc1iy',
    			),
            array(
                'nama' => 'petugas1.id', 
                'alamat' => 'Lambung Mangkurat',
                'no_telp' => '0836367333',
                'username' => 'petugas1',
                'password' => '$2y$10$ARd.LeGPzX6yunIXO6tnr.dfEZu1Og1ClWUEnjqzHDmshGq0p4jy2',
                )
    		);

    	DB::table('user')->insert($data);
    }
}
