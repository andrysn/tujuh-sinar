<?php

use Illuminate\Database\Seeder;

class HargaIndoorSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('harga_indoors')->delete();

    	DB::table('harga_indoors')->insert($data);
    }
}
