<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(PrinterSeed::class);
        $this->call(MemberSeed::class);
        $this->call(PelangganSeed::class);
        $this->call(SupplierSeed::class);
        $this->call(BarangSeed::class);
        $this->call(EditorsTableSeeder::class);
    }
}
