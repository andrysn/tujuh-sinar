<?php

use Illuminate\Database\Seeder;

class HargaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('hargas')->delete();

    	$data = array(
    		array(
    			'member_id' => '2',
                'produk_id' => '1',
                'keterangan' => 'Perusahaan Outdoor'
    			),
    		array(
    			'member_id' => '2',
                'produk_id' => '2',
                'keterangan' => 'Perusahaan Indoor'
                ),
            array(
    			'member_id' => '1',
                'produk_id' => '1',
                'keterangan' => 'Umum Outdoor'
    			)
    		);

    	DB::table('hargas')->insert($data);
    }
}
