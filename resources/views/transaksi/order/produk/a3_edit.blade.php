<form method="post" action="{{ route('storePrintQuarto') }}" >
<div id="a3_show" class="row col-md-12">
	{!! csrf_field() !!}
	{{ Form::hidden('harga', '', ['id' => 'print_harga']) }}
	{{ Form::hidden('produk_id', '4', ['id' => 'print_produk']) }}
	{{ Form::hidden('order', $order->order, ['id' => 'order']) }}
    {{ Form::hidden('pelanggan_id', $order->pelanggan_id, ['id' => 'print_pelanggan', 'oninput' => 'get_printer()']) }}

	<div class="col-lg-4">
		<div class="form-group">
			<label for="input3" class="form-label">Bahan</label>
			<select class="selectpicker form-control" name="barang_id" data-live-search="true" id="print_barang" onchange="get_printer()" required >
				<option selected>-- Pilih bahan --</option>
				@foreach($barangs as $barang)
					@if($barang->produk_id == 4)
						<option value="{{ $barang->id }}">{{ $barang->nm_barang }}</option>
					@endif
				@endforeach
			</select>
		</div>
	</div>
	
	<div class="col-md-1">
		<div class="form-group">
			<label class="form-label"></label>
			<fieldset>
				<div class="control-group">
					<div class="controls">
						<div class="input-prepend input-group" style="margin-top: -5px;">
							<div class="radio radio-danger">
                                <input type="radio" name="deadline_print" id="besok_print" value="besok" onclick="$('#pilih_deadline_print').attr('disabled','disabled');">
                                <label for="besok_print"> Besok </label>
                            </div>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
	</div>  

	<div class="col-md-1">
		<div class="form-group">
			<label class="form-label"></label>
			<fieldset>
				<div class="control-group">
					<div class="controls">
						<div class="input-prepend input-group" style="margin-top: -5px;">
							<div class="radio radio-danger">
                                <input type="radio" name="deadline_print" id="lusa_print" value="lusa" onclick="$('#pilih_deadline_print').attr('disabled','disabled');">
                                <label for="lusa_print"> Lusa </label>
                            </div>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
	</div> 

	<div class="col-md-6">
		<div class="form-group">
			<label class="form-label"></label>
			<fieldset>
				<div class="control-group">
					<div class="controls">
						<div class="input-prepend input-group" style="margin-top: 10px;">
							<div class="radio radio-danger col-lg-12">
                                <input type="radio" name="deadline_print" id="pilih_print" class="pilihan" value="pilih" onclick="$('#pilih_deadline_print').removeAttr('disabled');">
                                <label for="pilih_print"> Pilih </label>
                                <input type="date" class="form-control col-md-8" id="pilih_deadline_print" style="display: inline-block;" name="pilih_deadline_print" disabled="">
                            </div>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
	</div>  
	
	

	<div class="col-lg-2">
		<div class="form-group">
			<label for="input3" class="form-label"><input type="radio" name="ukuran" id="ukuran"  class="pilihan" value="pilih"  onclick="$('#ukuran_print_a3').removeAttr('disabled');$('#panjang_print').attr('disabled', 'disabled');$('#lebar_print').attr('disabled', 'disabled');"> <label for="ukuran">Ukuran</label> </label>
			<select class=" form-control" name="pilih_ukuran"  id="ukuran_print_a3" required disabled="" onchange="get_printer()">
				<option selected>-- Pilih Ukuran --</option>
				<option value="1">A4</option>
				<option value="2">F4</option>
				<option value="3">A3</option>
			</select>
		</div>
	</div>

	<div class="col-lg-2">
		<div class="form-group">
			<label for="inputp" class="form-label"><input type="radio" name="ukuran" value="input" id="inputPL" onclick="$('#ukuran_print_a3').attr('disabled','disabled');$('#panjang_print').removeAttr('disabled');$('#lebar_print').removeAttr('disabled');"> <label for="inputPL"> Panjang (Meter)</label></label>
			<input type="number" class="form-control" id="panjang_print" oninput="get_printer()" min="0" name="panjang" step="0.01" value="0.00" required disabled="">
		</div>
	</div>
	<div class="col-lg-2">
		<div class="form-group">
			<label for="inputl" class="form-label"><label for="inputPL"> Lebar (Meter)</label></label>
			<input type="number" class="form-control" id="lebar_print" oninput="get_printer()" min="0" name="lebar" step="0.01" value="0.00" required disabled="">
		</div>
	</div>
	
	<div class="col-lg-3" >
        <div class="form-group">
            <label for="input4" class="form-label">Tipe Print</label>
            <select class="form-control" name="tipe_print" id="pilih_tipe_print" required onchange="get_printer()">
                <option selected>--Pilih Tipe--</option>
                <option value="1">1 Sisi</option>
                <option value="2">2 Sisi</option>
            </select>
        </div>
    </div>
	
	<div class=" col-lg-3">
		<div class="form-group">
			<label for="" class="form-label">Finishing</label>
			<select class="form-control" name="editor_id" id="editor_print"    required onchange="get_printer()">
				<option selected>-- Pilih Finishing --</option>
				@foreach($editors as $editor)
					@if($editor->produk_id == 4)
						<option value="{{ $editor->id }}">{{ $editor->nama_finishing }}</option>
					@endif
				@endforeach
			</select>
		</div>
	</div>

	<div class="col-lg-3" >
		<div class="form-group">
			<label for="input4" class="form-label">Pilihan Kaki</label>
			<select class="form-control" name="kaki" id="kaki_print" required onchange="get_printer()">
				<option selected>--Pilih Kaki--</option>
				<option value="1">X-Banner</option>
				<option value="2">Roll Banner</option>
				<option value="3">Y Banner</option>
				<option value="4">Rollup</option>
				<option value="5">Toneup</option>
			</select>
		</div>
	</div>
	
	<div class="col-md-12 col-lg-2">
		<div class="form-group">
			<label for="input6" class="form-label">Qty</label>
			<input type="text" class="form-control" name="qty" oninput="get_printer()" id="print_qty" required>
		</div>
	</div>
	
			<input type="hidden" class="form-control" name="diskon" id="print_diskon" placeholder="%" readonly required>

	<div class="col-md-12 col-lg-2">
		
		<div class="form-group">
			<label for="" class="form-label">Total Harga</label>
			<input type="text" class="form-control" name="total" id="print_total" readonly placeholder="Total" required>
		</div>
	</div>

	<div class="col-md-12 col-lg-2">
		<button type="submit" class="btn btn-primary" id="PSub" disabled>Submit</button>
		<button type="reset" class="btn btn-square btn-danger"><i class="fa fa-undo"></i></button>
		<a href="{{url('admin/transaksi/order')}}" class="btn btn-default">Kembali</a>
	</div>

</div>
</form>

@push('style')
<script type="text/javascript">
    function get_printer() {
    	var ukuran = 0.00;
        var print_produk = document.getElementById("print_produk").value;
		var print_pelanggan = document.getElementById("print_pelanggan").value;
		
        var print_barang = document.getElementById("print_barang").value;
       
		var print_qty = document.getElementById("print_qty").value;
		var print_editor = document.getElementById("editor_print").value;
		
		var tipe_print = document.getElementById("pilih_tipe_print").value;
		var kaki_print = document.getElementById("kaki_print").value;
		if($('#ukuran').is(':checked')){
			switch($('#ukuran_print_a3').val()) {
			  case "1":
			    ukuran = 0.210 * 0.297;
			    break;
			  case "2":
			    ukuran = 0.215 * 0.33;
			    break;
			  case "3":
			    ukuran = 0.297 * 0.420;
			    break;
			  default:
			    ukuran = 0.00;
			}
		} else {
			ukuran =$('#panjang_print').val() * $('#lebar_print').val();
		}
	
		if(print_produk != '' && print_pelanggan != '' && print_barang != '' && print_qty != '' && print_editor != '' && tipe_print != '' ){
			jQuery.ajax({
	            url: "{{ url('admin/transaksi/order/print/data/') }}/"+print_barang+"/"+print_pelanggan+"/"+print_qty+"/"+ukuran+"/"+print_editor+"/"+tipe_print+"/"+kaki_print,
	            type: "GET",
	            success: function(data) {
	                jQuery('#print_diskon').val(data.diskon);
	                jQuery('#print_harga').val(data.harga);
	                jQuery('#print_total').val(data.total);
	                if(data.total > 0 || data.total != '') {
							jQuery('#PSub').removeAttr('disabled');
						} else {
							jQuery('#PSub').attr('disabled', 'disabled');
						}
	            }
			});
		}
	        

		
        
    }
</script>
@endpush