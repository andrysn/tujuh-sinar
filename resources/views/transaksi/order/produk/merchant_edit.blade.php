<form method="post" action="{{ route('storeMerchant') }}" >
<div id="display_show" class="row col-md-12">
	{!! csrf_field() !!}
	{{ Form::hidden('produk_id', '3', ['id' => 'merchant_produk']) }}
	{{ Form::hidden('order', $order->order, ['id' => 'order']) }}
	{{ Form::hidden('harga', '', ['id' => 'merchant_harga']) }}
    {{ Form::hidden('pelanggan_id', $order->pelanggan_id, ['id' => 'merchant_pelanggan', 'oninput' => 'get_display()']) }}
	
	<div class=" col-lg-4">
		<div class="form-group">
			<label for="input3" class="form-label">Bahan</label>
			<select class="selectpicker form-control" name="barang_id" data-live-search="true" id="merchant_barang" onchange="get_display()" required="">
				<option selected>-- Pilih bahan --</option>
				@foreach($barangs as $barang)
					@if($barang->produk_id == 3)
						<option value="{{ $barang->id }}">{{ $barang->nm_barang }}</option>
					@endif
				@endforeach
			</select>
		</div>
	</div>

	<div class="col-md-1">
		<div class="form-group">
			<label class="form-label"></label>
			<fieldset>
				<div class="control-group">
					<div class="controls">
						<div class="input-prepend input-group" style="margin-top: -5px;">
							<div class="radio radio-danger">
                                <input type="radio" name="deadline_merchant" id="besok_merchant" value="besok" onclick="$('#pilih_deadline_merchant').attr('disabled','disabled');">
                                <label for="besok_merchant"> Besok </label>
                            </div>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
	</div>  

	<div class="col-md-1">
		<div class="form-group">
			<label class="form-label"></label>
			<fieldset>
				<div class="control-group">
					<div class="controls">
						<div class="input-prepend input-group" style="margin-top: -5px;">
							<div class="radio radio-danger">
                                <input type="radio" name="deadline_merchant" id="lusa_merchant" value="lusa" onclick="$('#pilih_deadline_merchant').attr('disabled','disabled');">
                                <label for="lusa_merchant"> Lusa </label>
                            </div>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
	</div> 

	<div class="col-md-6">
		<div class="form-group">
			<label class="form-label"></label>
			<fieldset>
				<div class="control-group">
					<div class="controls">
						<div class="input-prepend input-group" style="margin-top: 10px;">
							<div class="radio radio-danger col-lg-12">
                                <input type="radio" name="deadline_merchant" id="pilih_merchant" value="pilih" onclick="$('#pilih_deadline_merchant').removeAttr('disabled');">
                                <label for="pilih_merchant"> Pilih </label>
                                <input type="date" class="form-control col-md-8" id="pilih_deadline_merchant" style="display: inline-block;" name="pilih_deadline_merchant" disabled="">
                            </div>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
	</div>  

	
	
	<div class="col-md-12 col-lg-2">
		<div class="form-group">
			<label for="input6" class="form-label">Qty</label>
			<input type="text" class="form-control" name="qty" oninput="get_display()" id="merchant_qty" required>
		</div>
	</div>
	
			<input type="hidden" class="form-control" name="diskon" id="merchant_diskon" placeholder="%" readonly required>

	<div class="col-md-12 col-lg-2">
		
		<div class="form-group">
			<label for="" class="form-label">Total Harga</label>
			<input type="text" class="form-control" name="total" id="merchant_total" readonly placeholder="Total" required>
		</div>
	</div>


	<div class="col-md-12 col-lg-4" style="margin-top: 28px;">
		<button type="submit" class="btn btn-primary" id="MSub" disabled>Submit</button>
		<button type="reset" class="btn btn-square btn-danger"><i class="fa fa-undo"></i></button>
		<a href="{{url('admin/transaksi/order')}}" class="btn btn-default">Kembali</a>
	</div>
</div>
</form>
@push('style')
<script type="text/javascript">

    function get_display() {
        var merchant_produk = document.getElementById("merchant_produk").value;
		var merchant_pelanggan = document.getElementById("merchant_pelanggan").value;
		
        var merchant_barang = document.getElementById("merchant_barang").value;
        
		var merchant_qty = document.getElementById("merchant_qty").value;

        jQuery.ajax({
            url: "{{ url('admin/transaksi/order/merchant/data/') }}/"+merchant_barang+"/"+merchant_pelanggan+"/"+merchant_qty,
            type: "GET",
            success: function(data) {
                jQuery('#merchant_diskon').val(data.diskon);
                jQuery('#merchant_total').val(data.total);
                jQuery('#merchant_harga').val(data.harga);
                if(data.total > 0 || data.total != '') {
							jQuery('#MSub').removeAttr('disabled');
						} else {
							jQuery('#MSub').attr('disabled', 'disabled');
						}
            }
		});
		
		
        
    }
</script>
@endpush