<form method="post" action="{{ route('storeIndoor') }}" >
<div id="indoor_show" class="row col-md-12">
	{!! csrf_field() !!}
	{{ Form::hidden('harga', '', ['id' => 'harga_indoor']) }}
	{{ Form::hidden('produk_id', '2', ['id' => 'produk_indoor']) }}
	<div class="col-md-12 col-lg-2">
		<div class="form-group">
			<label for="input2" class="form-label">No Order</label>
			<input type="text" class="form-control" name="order" required readonly value="{{ $kode }}">
		</div>
	</div> 
	<div class="col-md-12 col-lg-3">
		<div class="form-group">
			<label for="select2" class="form-label">Customer</label>
			<select class="selectpicker form-control" name="pelanggan_id" data-live-search="true" id="pelanggan_indoor" onchange="get_indoor()" required>
				<option disable>-- Pilih Member --</option>
				@foreach($members as $member)
				<option value="{{ $member->id }}">{{ $member->id }} - {{ $member->nama }} [{{ $member->member->nm_tipe }}] - {{ $member->no_telp }}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="form-group">
			<label class="form-label">Nama File</label>
			<input type="text" name="nama_file" class="form-control">
		</div>
	</div>

	<div class="col-lg-3">
		<div class="form-group">
			<label for="input3" class="form-label">Barang</label>
			<select class="selectpicker form-control" name="barang_id" data-live-search="true" onchange="get_indoor()" id="barang_indoor" required>
				<option disable>-- Pilih Barang --</option>
				@foreach($barangs as $barang)
					@if($barang->produk_id == 2)
						<option value="{{ $barang->id }}">{{ $barang->nm_barang }}</option>
					@endif
				@endforeach
			</select>
		</div>
	</div>
	
	<div class="col-md-1">
		<div class="form-group">
			<label class="form-label"></label>
			<fieldset>
				<div class="control-group">
					<div class="controls">
						<div class="input-prepend input-group" style="margin-top: -5px;">
							<div class="radio radio-danger">
                                <input type="radio" name="deadline_indoor" id="besok_indoor" value="besok" onclick="$('#pilih_deadline_indoor').attr('disabled','disabled');">
                                <label for="besok_indoor"> Besok </label>
                            </div>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
	</div>  

	<div class="col-md-1">
		<div class="form-group">
			<label class="form-label"></label>
			<fieldset>
				<div class="control-group">
					<div class="controls">
						<div class="input-prepend input-group" style="margin-top: -5px;">
							<div class="radio radio-danger">
                                <input type="radio" name="deadline_indoor" id="lusa_indoor" value="lusa" onclick="$('#pilih_deadline_indoor').attr('disabled','disabled');">
                                <label for="lusa_indoor"> Lusa </label>
                            </div>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
	</div> 

	<div class="col-md-6">
		<div class="form-group">
			<label class="form-label"></label>
			<fieldset>
				<div class="control-group">
					<div class="controls">
						<div class="input-prepend input-group" style="margin-top: 10px;">
							<div class="radio radio-danger col-lg-12">
                                <input type="radio" name="deadline_indoor" id="pilih_indoor" value="pilih" onclick="$('#pilih_deadline_indoor').removeAttr('disabled');">
                                <label for="pilih_indoor"> Pilih </label>
                                <input type="date" class="form-control col-md-8" id="pilih_deadline_indoor" style="display: inline-block;" name="pilih_deadline_indoor" disabled="">
                            </div>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
	</div>  
	

	<div class="col-lg-2">
		<div class="form-group">
			<label for="inputp" class="form-label">Panjang (Meter)</label>
			<input type="number" class="form-control" id="panjang_indoor" oninput="get_indoor()" value="1.00" min="0" name="panjang" step="0.01" required>
		</div>
	</div>
	<div class="col-lg-2">
		<div class="form-group">
			<label for="inputl" class="form-label">Lebar (Meter)</label>
			<input type="number" class="form-control" id="lebar_indoor" oninput="get_indoor()" value="1.00" min="0" name="lebar" step="0.01" required>
		</div>
	</div>
	
	
	<div class=" col-lg-3">
		<div class="form-group">
			<label for="" class="form-label">Finishing</label>
			<select class="form-control" name="editor_id" id="editor_indoor"    required onchange="get_indoor()">
				<option selected>-- Pilih Finishing --</option>
				@foreach($editors as $editor)
					@if($editor->produk_id == 2)
						<option value="{{ $editor->id }}">{{ $editor->nama_finishing }}</option>
					@endif
				@endforeach
			</select>
		</div>
	</div>

	<div class="col-lg-3" >
        <div class="form-group">
            <label for="input4" class="form-label">Tipe Print</label>
            <select class="form-control" name="tipe_print" id="tipe_print" required onchange="get_indoor()">
                <option selected>--Pilih Tipe--</option>
                <option value="1">Print Only</option>
                <option value="2">Cut Only</option>
                <option value="3">Print + Cut</option>
            </select>
        </div>
    </div>
    
	<div class="col-md-12 col-lg-2">
		<div class="form-group">
			<label for="input6" class="form-label">Qty</label>
			<input type="text" class="form-control" name="qty" oninput="get_indoor()" id="qty_indoor" required>
		</div>
	</div>
	
			<input type="hidden" class="form-control" name="diskon" id="diskon_indoor" placeholder="%" readonly required>

	<div class="col-md-12 col-lg-2">
		
		<div class="form-group">
			<label for="" class="form-label">Total Harga</label>
			<input type="text" class="form-control" name="total" id="total_indoor" readonly placeholder="Total" required>
		</div>
	</div>

	<div class="col-md-12 col-lg-4" style="margin-top: 28px;">
		<button type="submit" class="btn btn-primary" id="ISub" disabled>Submit</button>
		<button type="reset" class="btn btn-square btn-danger"><i class="fa fa-undo"></i></button>
		<a href="{{url('admin/transaksi/order')}}" class="btn btn-default">Kembali</a>
	</div>
</div>
</form>


@push('style')
<script type="text/javascript">

    function get_indoor() {
    	var produk_indoor = document.getElementById("produk_indoor").value;
		var pelanggan_indoor = document.getElementById("pelanggan_indoor").value;
  		var barang_indoor = document.getElementById("barang_indoor").value;
  		var editor_indoor = document.getElementById("editor_indoor").value;
		var qty_indoor = document.getElementById("qty_indoor").value;
		var p_indoor = document.getElementById("panjang_indoor").value;
		var l_indoor = document.getElementById("lebar_indoor").value;
		var tipe_print = document.getElementById("tipe_print").value;

		if(produk_indoor != '' && pelanggan_indoor != '' && barang_indoor != '' && editor_indoor != '' && qty_indoor != '' && l_indoor != '' && p_indoor != '' && produk_indoor != null && pelanggan_indoor != null && barang_indoor != null && editor_indoor != null && qty_indoor != null && l_indoor != null && p_indoor != null){

			jQuery.ajax({
	        	
            url: "{{ url('admin/transaksi/order/indoor/data/') }}/"+barang_indoor+"/"+editor_indoor+"/"+pelanggan_indoor+"/"+qty_indoor+"/"+p_indoor+"/"+l_indoor+"/"+tipe_print,
	            type: "GET",
	            success: function(data) {
	                jQuery('#diskon_indoor').val(data.diskon);
	                jQuery('#total_indoor').val(data.total);
	                jQuery('#harga_indoor').val(data.harga);
	                if(data.total > 0 || data.total != '') {
							jQuery('#ISub').removeAttr('disabled');
						} else {
							jQuery('#ISub').attr('disabled', 'disabled');
						}
	            }
			});
			

		}
      

     
        
    }
</script>
@endpush