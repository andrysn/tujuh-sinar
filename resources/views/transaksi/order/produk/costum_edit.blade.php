<form method="post" action="{{ route('storeCostumProduk') }}" >
<div id="costum_show" class="row col-md-12">
	{!! csrf_field() !!}
	{{ Form::hidden('harga', '', ['id' => 'custom_harga']) }}
	{{ Form::hidden('produk_id', '6', ['id' => 'custom_produk']) }}
	{{ Form::hidden('order', $order->order, ['id' => 'order']) }}

    {{ Form::hidden('pelanggan_id', $order->pelanggan_id, ['id' => 'custom_pelanggan', 'oninput' => 'get_costum()']) }}

	<div class="col-md-1">
		<div class="form-group">
			<label class="form-label"></label>
			<fieldset>
				<div class="control-group">
					<div class="controls">
						<div class="input-prepend input-group" style="margin-top: -5px;">
							<div class="radio radio-danger">
                                <input type="radio" name="deadline_custom" id="besok_custom" value="besok" onclick="$('#pilih_deadline_custom').attr('disabled','disabled');">
                                <label for="besok_custom"> Besok </label>
                            </div>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
	</div>  

	<div class="col-md-1">
		<div class="form-group">
			<label class="form-label"></label>
			<fieldset>
				<div class="control-group">
					<div class="controls">
						<div class="input-prepend input-group" style="margin-top: -5px;">
							<div class="radio radio-danger">
                                <input type="radio" name="deadline_custom" id="lusa_custom" value="lusa" onclick="$('#pilih_deadline_custom').attr('disabled','disabled');">
                                <label for="lusa_custom"> Lusa </label>
                            </div>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
	</div> 

	<div class="col-md-5">
		<div class="form-group">
			<label class="form-label"></label>
			<fieldset>
				<div class="control-group">
					<div class="controls">
						<div class="input-prepend input-group" style="margin-top: 10px;">
							<div class="radio radio-danger col-lg-12">
                                <input type="radio" name="deadline_custom" id="pilih_custom" class="pilihan" value="pilih" onclick="$('#pilih_deadline_custom').removeAttr('disabled');">
                                <label for="pilih_custom"> Pilih </label>
                                <input type="date" class="form-control col-md-8" id="pilih_deadline_custom" style="display: inline-block;" name="pilih_deadline_custom" disabled="">
                            </div>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
	</div>  
	<div class="col-md-12 col-lg-2">
		<div class="form-group">
			<label for="input5" class="form-label">Nama Produk</label>
			<input type="text" class="form-control" name="nama_produk" id="custom_nama_produk" placeholder="Nama Produk" onchange="get_costum()" required>
		</div>
	</div>

	<div class="col-md-12 col-lg-2">
		<div class="form-group">
			<label for="input6" class="form-label">Qty</label>
			<input type="text" class="form-control" name="qty" oninput="get_costum()" id="custom_qty" required>
		</div>
	</div>
	
	<div class="col-md-12 col-lg-1">
		<div class="form-group">
			<label for="input5" class="form-label">Diskon</label>
			<input type="text" class="form-control" name="diskon" id="custom_diskon" placeholder="%" readonly required>
		</div>
	</div>

			<input type="hidden" class="form-control" name="total" id="custom_total" readonly placeholder="Total" required>

	<div class="col-md-12 col-lg-4">
		<div class="form-group">
			<label for="inputext" class="form-label">Keterangan</label>
			<textarea class="form-control" id="inputext" name="keterangan"></textarea>
		</div>
	</div>

	<div class="col-md-12 col-lg-4" style="margin-top: 28px;">
		<button type="submit" class="btn btn-primary" id="CSub" disabled>Submit</button>
		<button type="reset" class="btn btn-square btn-danger"><i class="fa fa-undo"></i></button>
		<a href="{{url('admin/transaksi/order')}}" class="btn btn-default">Kembali</a>
	</div>

</div>
</form>

@push('style')
<script type="text/javascript">

    function get_costum() {
        var custom_produk = document.getElementById("custom_produk").value;
		var custom_pelanggan = document.getElementById("custom_pelanggan").value;
        var custom_nama_produk = document.getElementById("custom_nama_produk").value;
		var custom_qty = document.getElementById("custom_qty").value;

        jQuery.ajax({
            url: "{{ url('admin/transaksi/order/costum/data/') }}/"+custom_nama_produk+"/"+custom_produk+"/"+custom_pelanggan+"/"+custom_qty,
            type: "GET",
            success: function(data) {
                jQuery('#custom_diskon').val(data.diskon);
                jQuery('#custom_harga').val(data.harga);
                jQuery('#custom_total').val(data.total);
                if(data.total > 0 || data.total != '') {
							jQuery('#CSub').removeAttr('disabled');
						} else {
							jQuery('#CSub').attr('disabled', 'disabled');
						}
            }
		});

		
        
    }
</script>
@endpush