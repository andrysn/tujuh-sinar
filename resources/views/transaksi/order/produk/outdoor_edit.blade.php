<form method="post" action="{{ route('storeOutdoor') }}" >
<div id="kartu_nama_show" class="row col-md-12">
	{!! csrf_field() !!}
	{{ Form::hidden('produk_id', '1', ['id' => 'produk']) }}
	{{ Form::hidden('harga', '', ['id' => 'harga']) }}
	{{ Form::hidden('order', $order->order, ['id' => 'order']) }}
    {{ Form::hidden('pelanggan_id', $order->pelanggan_id, ['id' => 'pelanggan', 'oninput' => 'get_card_name()']) }}
	<div class="col-lg-4">
		<div class="form-group">
			<label class="form-label">Nama File</label>
			<input type="text" name="nama_file" class="form-control">
		</div>
	</div>

	<div class="col-lg-3">
		<div class="form-group">
			<label for="input3" class="form-label">Barang</label>
			<select class="selectpicker form-control" name="barang_id" data-live-search="true" onchange="get_card_name()" id="barang" required>
				<option disable>-- Pilih Barang --</option>
				@foreach($barangs as $barang)
					@if($barang->produk_id == 1)
						<option value="{{ $barang->id }}">{{ $barang->nm_barang }}</option>
					@endif
				@endforeach
			</select>
		</div>
	</div>
	
	<div class="col-md-1">
		<div class="form-group">
			<label class="form-label"></label>
			<fieldset>
				<div class="control-group">
					<div class="controls">
						<div class="input-prepend input-group" style="margin-top: -5px;">
							<div class="radio radio-danger">
                                <input type="radio" name="deadline" id="beosk_outdoor" value="besok" onclick="$('#pilih_deadline_outdoor').attr('disabled','disabled');">
                                <label for="beosk_outdoor"> Besok </label>
                            </div>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
	</div>  

	<div class="col-md-1">
		<div class="form-group">
			<label class="form-label"></label>
			<fieldset>
				<div class="control-group">
					<div class="controls">
						<div class="input-prepend input-group" style="margin-top: -5px;">
							<div class="radio radio-danger">
                                <input type="radio" name="deadline" id="lusa_outdoor" value="lusa" onclick="$('#pilih_deadline_outdoor').attr('disabled','disabled');">
                                <label for="lusa_outdoor"> Lusa </label>
                            </div>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
	</div> 

	<div class="col-md-6">
		<div class="form-group">
			<label class="form-label"></label>
			<fieldset>
				<div class="control-group">
					<div class="controls">
						<div class="input-prepend input-group" style="margin-top: 10px;">
							<div class="radio radio-danger col-lg-12">
                                <input type="radio" name="deadline" id="pilih_outdoor" value="pilih" onclick="$('#pilih_deadline_outdoor').removeAttr('disabled');">
                                <label for="pilih_outdoor"> Pilih </label>
                                <input type="date" class="form-control col-md-8" id="pilih_deadline_outdoor" style="display: inline-block;" name="pilih_deadline_outdoor" disabled="">
                            </div>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
	</div>  
	

	<div class="col-lg-2">
		<div class="form-group">
			<label for="inputp" class="form-label">Panjang (Meter)</label>
			<input type="number" class="form-control" id="panjang" oninput="get_card_name()" value="1.00" min="0" name="panjang" step="0.01" value="0.00" required>
		</div>
	</div>
	<div class="col-lg-2">
		<div class="form-group">
			<label for="inputl" class="form-label">Lebar (Meter)</label>
			<input type="number" class="form-control" id="lebar" oninput="get_card_name()" value="1.00" min="0" name="lebar" step="0.01" value="0.00" required>
		</div>
	</div>
	
	
	<div class=" col-lg-3">
		<div class="form-group">
			<label for="" class="form-label">Finishing</label>
			<select class="form-control" name="editor_id" id="editor_outdoor"    required onchange="get_card_name()">
				<option selected>-- Pilih Finishing --</option>
				@foreach($editors as $editor)
					@if($editor->produk_id == 1)
						<option value="{{ $editor->id }}">{{ $editor->nama_finishing }}</option>
					@endif
				@endforeach
			</select>
		</div>
	</div>

	<div class="col-lg-2">
		<div class="form-group">
			<label for="input6" class="form-label">Qty</label>
			<input type="text" class="form-control" name="qty" oninput="get_card_name()" id="qty" required>
		</div>
	</div>
	
			<input type="hidden" class="form-control" name="diskon" id="diskon" min="0" max="100" value="0" placeholder="%" readonly required>

	<div class="col-lg-2">
		
		<div class="form-group">
			<label for="" class="form-label">Total Harga</label>
			<input type="text" class="form-control" name="total" id="total" readonly placeholder="Total" required>
		</div>
	</div>

	<div class="col-md-12 col-lg-4" style="margin-top: 28px;">
		<button type="submit" class="btn btn-primary" id="OSub" disabled>Submit</button>
		<button type="reset" class="btn btn-square btn-danger"><i class="fa fa-undo"></i></button>
		<a href="{{url('admin/transaksi/order')}}" class="btn btn-default">Kembali</a>
	</div>
</div>
</form>

@push('style')
<script type="text/javascript">

    
    function get_card_name() {
        var produk = document.getElementById("produk").value;
		var pelanggan = document.getElementById("pelanggan").value;
  		var barang = document.getElementById("barang").value;
  		var editor = document.getElementById("editor_outdoor").value;
		var qty = document.getElementById("qty").value;
		var p = document.getElementById("panjang").value;
		var l = document.getElementById("lebar").value;

		if(produk != '' && pelanggan != '' && barang != '' && editor != '' && qty != '' && l != '' && p != '' && produk != null && pelanggan != null && barang != null && editor != null && qty != null && l != null && p != null){

			jQuery.ajax({
	        	
	            url: "{{ url('admin/transaksi/order/outdoor/data/') }}/"+barang+"/"+editor+"/"+pelanggan+"/"+qty+"/"+p+"/"+l,
	            type: "GET",
	            success: function(data) {
	                jQuery('#diskon').val(data.diskon);
	                jQuery('#total').val(data.total);
	                jQuery('#harga').val(data.harga);
	                if(data.total > 0 || data.total != '') {
							jQuery('#OSub').removeAttr('disabled');
						} else {
							jQuery('#OSub').attr('disabled', 'disabled');
						}
	            }
			});
			

		
		}
	        

        
    }
</script>
@endpush