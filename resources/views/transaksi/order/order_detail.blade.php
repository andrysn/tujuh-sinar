@extends('layout.layout')
@section('title', 'Tambah Data Order')
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-title"> Total Harga
                <ul class="panel-tools">
                    <li><a class="icon minimise-tool"><i class="fa fa-minus"></i></a></li>
                </ul> 
            </div>
            <div class="panel-body">
                <h1>Rp. {{ number_format($totalan->total) }}</h1>
            </div>
        </div>
    </div>
</div>
<div class="row"> 
    <!-- Start Panel -->
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-title"> Data Pekerjaan </div>
            @if($order->status_produksi < '3')
            <a href="{{ route('order.edit', $id) }}" class="btn btn-default"><i class="fa fa-plus-circle"></i>Tambah Order</a>
            @endif
               <a href="{{ route('order.index') }}" class="btn btn-default">Kembali</a>

            <br>
            <br>
            <div class="panel-body table-responsive">
                <table id="example0" class="table display">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Deadline</th>
                            <th>Produk</th>
                            <th>Bahan</th>
                            <th>Diskon</th>
                            <th>Qty</th>
                            <th>Total Harga</th>
                            <th>Keterangan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($data as $index => $datas)
                        <tr>
                            <td>{{ $index + 1 }}</td>
                            <th>{{ $datas->tgl_deadline }}</th>
                            <td>{{ $datas->nama_produk }}</td>
                            <td>{{ is_null($datas->barang_id) ? '-' : $datas->barang->nm_barang }}</td>
                            <td>{{ $datas->diskon }} %</td>
                            <td>{{ $datas->qty }}</td>
                            <td>Rp. {{ number_format($datas->total) }}</td>
                            <td>{!! $datas->keterangan !!}</td>
                        </tr>
                        @empty
                        @endforelse
                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <!-- End Panel --> 
</div>

@endsection