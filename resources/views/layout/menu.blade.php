<!-- Start Sidabar -->
<div class="sidebar clearfix">
    <ul class="sidebar-panel nav metismenu" id="side-menu" data-plugin="metismenu">
        @if(Auth::user()->role == 1)
        <li class="active"><a href="{{ url('/admin/dashboard') }}"><span class="icon color5"><i class="fa fa-home" aria-hidden="true"></i></span><span class="nav-title">Dashboard</span></a>
        </li>
        <li><a href="#"><span class="icon color10"><i class="fa fa-check-square-o" aria-hidden="true"></i></span>Master Data <i class="fa fa-sort-desc caret"></i></a>
            <ul>
                <li><a href="{{ route('tools.index') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Material & Alat</a></li>
                <li><a href="{{ route('member.index') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Data Member</a></li>
                <li><a href="{{ route('harga.index') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Data Harga</a></li>
                <li><a href="{{ route('costumer.index') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Data Costumer</a></li>
                <li><a href="{{ route('supplier.index') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Data Supplier</a></li>
                <li><a href="{{ route('pengguna.index') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Data Pengguna</a></li>
                {{-- <li><a href="{{ route('editor.index') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Data Editor</a></li> --}}
            </ul>
        </li> 
        <li><a href="#"><span class="icon color5"><i class="fa fa-shopping-cart" aria-hidden="true"></i></span><span class="nav-title">Transaksi</span><i class="fa fa-sort-desc caret"></i></a>
           <ul>
            <li><a href="{{ route('antrian.index') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Antrian</a></li>
            <li><a href="{{ route('order.index') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Order Kerja</a></li>
            <li><a href="{{ route('produksi.index') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Produksi</a></li>
            <li><a href="{{ route('gudang.index') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Gudang</a></li>
        </ul>
    </li>
    <li><a href="#"><span class="icon color5"><i class="fa fa-money" aria-hidden="true"></i></span><span class="nav-title">Keuangan</span><i class="fa fa-sort-desc caret"></i></a>
       <ul>
        <li><a href="{{ route('pembelian.index') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Pembelian</a></li>
        <li><a href="{{route('piutang.index')}}"><i class="fa fa-angle-right" aria-hidden="true"></i> Data Piutang</a></li>
        <li><a href="{{ route('utang') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Data Utang</a></li>
    </ul>
</li>
<li><a href="#"><span class="icon color5"><i class="fa fa-list" aria-hidden="true"></i></span><span class="nav-title">Laporan</span><i class="fa fa-sort-desc caret"></i></a>
   <ul>
    <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Penjualan</a></li>
    <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Keuangan</a></li>
    <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Produksi</a></li>
    <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Status Order</a></li>
    <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Stok Barang</a></li>
    <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Hutang Supplier</a></li>
    <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Pembayaran</a></li>
    <li><a href="{{route('lapbeli')}}"><i class="fa fa-angle-right" aria-hidden="true"></i> Pembelian</a></li>
    <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Keluar Barang</a></li>
</ul>
</li>
@elseif(Auth::user()->role == 2)
<li class="active"><a href="{{ url('/admin/dashboard') }}"><span class="icon color5"><i class="fa fa-home" aria-hidden="true"></i></span><span class="nav-title">Dashboard</span></a>
</li>
@elseif(Auth::user()->role == 3)
<li><a href="#"><span class="icon color5"><i class="fa fa-shopping-cart" aria-hidden="true"></i></span><span class="nav-title">Transaksi</span><i class="fa fa-sort-desc caret"></i></a>
   <ul>
    <li><a href="{{ route('produksi.index') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Produksi</a></li>
</ul>
</li>
@elseif(Auth::user()->role == 4)
<li class="active"><a href="{{ url('/admin/dashboard') }}"><span class="icon color5"><i class="fa fa-home" aria-hidden="true"></i></span><span class="nav-title">Dashboard</span></a>
</li>
<li><a href="#"><span class="icon color5"><i class="fa fa-money" aria-hidden="true"></i></span><span class="nav-title">Keuangan</span><i class="fa fa-sort-desc caret"></i></a>
    <ul>
        <li><a href="{{route('piutang.index')}}"><i class="fa fa-angle-right" aria-hidden="true"></i> Data Piutang</a></li>
    </ul>
</li>
@elseif(Auth::user()->role == 5)
<li><a href="#"><span class="icon color5"><i class="fa fa-shopping-cart" aria-hidden="true"></i></span><span class="nav-title">Transaksi</span><i class="fa fa-sort-desc caret"></i></a>
   <ul>
    <li><a href="{{ route('antrian.index') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Antrian</a></li>
    <li><a href="{{ route('order.index') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Order Kerja</a></li>
</ul>
</li>
@endif
<li><a href="#"><span class="icon color5"><i class="fa fa-gear" aria-hidden="true"></i></span><span class="nav-title">Pengaturan</span><i class="fa fa-sort-desc caret"></i></a>
   <ul>
    <!-- <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Profil</a></li> -->
    <li>
        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> Logout </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}</form>
    </li>
</ul>
</li>
</ul>
</div>
                        <!-- End Sidabar -->