<form method="post" action="{{ route('harga.printQuarto') }}" >
<div id="a3_show" class="row ">
	<div class="row col-lg-12">
		<div class="col-lg-12">
			<h3>Print A3</h3>
		</div>
		
	</div>
	{!! csrf_field() !!}
	<div class="col-md-12 col-lg-3">
		<div class="form-group">
			<label for="select2" class="form-label">Tipe Member</label>
			<select class="form-control" name="member_id" required>
				<option value="">-- Pilih Member --</option>
				@foreach($member as $tampil)
				<option value="{{ $tampil->id }}">{{ $tampil->nm_tipe }}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="col-lg-3" >
        <div class="form-group">
            <label for="input4" class="form-label">Tipe Print</label>
            <select class="form-control" name="tipe_print" id="input_kn" required>
                <option selected>--Pilih Tipe--</option>
                <option value="1">1 Sisi</option>
                <option value="2">2 Sisi</option>
            </select>
        </div>
    </div>
	<div class="col-lg-3" >
		<div class="form-group">
			<label for="input4" class="form-label">Pilihan Kaki</label>
			<select class="form-control" name="kaki" id="input_ds" required>
				<option selected>--Pilih Kaki--</option>
				<option value="1">X-Banner</option>
				<option value="2">Roll Banner</option>
				<option value="3">Y Banner</option>
				<option value="4">Rollup</option>
				<option value="5">Toneup</option>
			</select>
		</div>
	</div>

	<div class="col-lg-3">
		<div class="form-group">
			<label for="input3" class="form-label">Barang</label>
			<select class="form-control" name="barang_id" id="input_kn" required>
				<option selected>-- Pilih bahan --</option>
				@foreach($barangs as $barang)
					@if($barang->produk_id == 4)
						<option value="{{ $barang->id }}">{{ $barang->nm_barang }}</option>
					@endif
				@endforeach
			</select>
		</div>
	</div>

	<div class=" col-lg-10">
		<div class="form-group">
			<label for="" class="form-label">Finishing</label>
			<div class="row col-md-12">
				<div class="icheck-input">
					<label for="editor_a3_label" class="col-md-4">Pilih Finishing</label>
					<input class="i-min-check" class="col-md-1"  onclick="$('#nama_finishing_a3').attr('disabled','disabled');$('#editor_a3').removeAttr('disabled');" type="radio" id="editor_a3_label"  name="finishing" value="old">
					<label for="editor_a3" class="col-md-7">
						<select class="form-control" name="editor_id" id="editor_a3" disabled="disabled"   required>
							<option selected>-- Pilih Finishing --</option>
							@foreach($editors as $editor)
								@if($editor->produk_id == 4)
									<option value="{{ $editor->id }}">{{ $editor->nama_finishing }}</option>
								@endif
							@endforeach
						</select>
					</label>
				</div>	
				<div class="icheck-input">
					<label for="nama_finishing_a3_label" class="col-md-4">Finishing Baru</label>
					<input class="i-min-check" class="col-md-1" onclick="$('#editor_a3').attr('disabled','disabled');$('#nama_finishing_a3').removeAttr('disabled');" type="radio" id="nama_finishing_a3_label"  name="finishing" value="new">
					<label for="nama_finishing_a3" class="col-md-7">
						<input type="text" disabled="disabled" class="form-control manual" name="nama_finishing" id="nama_finishing_a3" placeholder="Nama Finishing Baru"/>
					</label>
				</div>					
			</div>
		</div>
	</div>
	
	<div class="col-md-12 col-lg-3">
		<div class="form-group">
			<label for="input" class="form-label">keterangan</label>
			<input type="text" class="form-control" name="keterangan" id="input">
		</div>
	</div>
	
	<div class="col-md-12 col-lg-3">
		<div class="form-group">
			<label for="" class="form-label">Harga</label>
			<div class="row">
				<div class="col-md-12 col-lg-6">
					<input type="text" class="form-control" name="harga_pokok" min="0" max="99999999" id="input_kn" placeholder="Pokok" required>
				</div>
				<div class="col-md-12 col-lg-6">
					<input type="text" class="form-control" name="harga_jual" min="0" max="99999999" id="input_kn" placeholder="Jual" required>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-md-12 col-lg-1">
		<div class="form-group">
			<label for="input5" class="form-label">Diskon</label>
			<input type="text" class="form-control" name="disc" id="input_kn" value="0" min="0" placeholder="%" required>
		</div>
	</div>

	<div class="col-md-12 col-lg-3">
		<div class="form-group">
			<label for="" class="form-label">Range Quantity</label>
			<div class="row">
				<div class="col-md-12 col-lg-6">
					<input type="number" class="form-control" name="range_min" min="0" id="input_kn" placeholder="Min" required>
				</div>
				<div class="col-md-12 col-lg-6">
					<input type="number" class="form-control" name="range_max" min="1" id="input_kn" placeholder="Max" required>
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-12 col-lg-3">
		<button type="submit" class="btn btn-primary">Submit</button>
		<button onclick="goBack()" class="btn btn-default">Kembali</button>
	</div>

</div>
</form>