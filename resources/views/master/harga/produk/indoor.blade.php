<form method="post" action="{{ route('harga.indoor') }}" >
<div id="flyer_show" class="row ">
	<div class="row col-lg-12">
		<div class="col-lg-12">
			<h3>Indoor</h3>
		</div>
		
	</div>
	{!! csrf_field() !!}
	<div class="col-lg-3">
		<div class="form-group">
			<label for="select2" class="form-label">Tipe Member</label>
			<select class="form-control" name="member_id" required>
				<option value="">-- Pilih Member --</option>
				@foreach($member as $tampil)
				<option value="{{ $tampil->id }}">{{ $tampil->nm_tipe }}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="col-lg-3">
		<div class="form-group">
			<label for="input" class="form-label">keterangan</label>
			<input type="text" class="form-control" name="keterangan" id="input">
		</div>
	</div>


	<div class="col-lg-3">
		<div class="form-group">
			<label for="input3" class="form-label">Barang</label>
			<select class="form-control" name="barang_id" id="input_fly" required="">
				<option selected>-- Pilih bahan --</option>
				@foreach($barangs as $barang)
					@if($barang->produk_id == 2)
						<option value="{{ $barang->id }}">{{ $barang->nm_barang }}</option>
					@endif
				@endforeach
			</select>
		</div>
	</div>


    <div class="col-lg-3" >
        <div class="form-group">
            <label for="input4" class="form-label">Tipe Print</label>
            <select class="form-control" name="tipe_print" id="tipe_print" required>
                <option selected>--Pilih Tipe--</option>
                <option value="1">Print Only</option>
                <option value="2">Cut Only</option>
                <option value="3">Print + Cut</option>
            </select>
        </div>
    </div>
	
	<div class=" col-lg-12">
		<div class="form-group">
			<label class="form-label">Finishing</label>
			<div class="row col-md-12">
				<div class="icheck-input">
					<label  for="editor_indoor_label" class="col-md-5">Pilih Finishing</label>
					<input class="i-min-check" class="col-md-1"  onclick="$('#nama_finishing_indoor').attr('disabled','disabled');$('#editor_indoor').removeAttr('disabled');" type="radio" id="editor_indoor_label"  name="finishing" value="old">
					<label for="editor_indoor" class="col-md-6">
						<select class="form-control" name="editor_id" id="editor_indoor" disabled="disabled"   required>
							<option selected>-- Pilih Finishing --</option>
							@foreach($editors as $editor)
								@if($editor->produk_id == 2)
									<option value="{{ $editor->id }}">{{ $editor->nama_finishing }}</option>
								@endif
							@endforeach
						</select>
					</label>
				</div>	
				<div class="icheck-input">
					<label for="nama_finishing_indoor_label" class="col-md-4">Finishing Baru</label>
					<input class="i-min-check" class="col-md-1" onclick="$('#editor_indoor').attr('disabled','disabled');$('#nama_finishing_indoor').removeAttr('disabled');" type="radio" id="nama_finishing_indoor_label"  name="finishing" value="new">
					<label for="nama_finishing_indoor" class="col-md-7">
						<input type="text" disabled="disabled" class="form-control manual" name="nama_finishing" id="nama_finishing_indoor" placeholder="Nama Finishing Baru"/>
					</label>
				</div>	

			
				
			</div>
		</div>
	</div>

	<div class="col-lg-4">
		<div class="form-group">
			<label for="" class="form-label">Harga</label>
			<div class="row">
				<div class="col-md-12 col-lg-6">
					<input type="text" class="form-control" name="harga_pokok" min="0" max="99999999" id="input_kn" placeholder="Pokok" required>
				</div>
				<div class="col-md-12 col-lg-6">
					<input type="text" class="form-control" name="harga_jual" min="0" max="99999999" id="input_kn" placeholder="Jual" required>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-md-12 col-lg-1">
		<div class="form-group">
			<label for="input5" class="form-label">Diskon</label>
			<input type="text" class="form-control" name="disc" id="input_kn" min="0" value="0" placeholder="%" required>
		</div>
	</div>

	<div class="col-md-12 col-lg-3">
		<div class="form-group">
			<label for="" class="form-label">Range Quantity</label>
			<div class="row">
				<div class="col-md-12 col-lg-6">
					<input type="number" class="form-control" name="range_min" min="0" id="input_kn" placeholder="Min" required>
				</div>
				<div class="col-md-12 col-lg-6">
					<input type="number" class="form-control" name="range_max" min="1" id="input_kn" placeholder="Max" required>
				</div>
			</div>
		</div>
	</div>

	<div class="col-lg-4" style="margin-top: 28px">
		<button type="submit" class="btn btn-primary">Submit</button>
		<button onclick="goBack()" class="btn btn-default">Kembali</button>
	</div>
</div>
</form>