<form method="post" action="{{ route('harga.outdoor') }}" >
<div id="kartu_nama_show" class="row ">
	<div class="row col-lg-12">
		<div class="col-lg-12">
			<h3>Outdoor</h3>
		</div>
		
	</div>
	{!! csrf_field() !!}
	<div class="col-lg-3">
		<div class="form-group">
			<label for="select2" class="form-label">Tipe Member</label>
			<select class="form-control" name="member_id" required>
				<option value="">-- Pilih Member --</option>
				@foreach($member as $tampil)
				<option value="{{ $tampil->id }}">{{ $tampil->nm_tipe }}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class=" col-lg-3">
		<div class="form-group">
			<label for="input" class="form-label">keterangan</label>
			<input type="text" class="form-control" name="keterangan" id="input">
		</div>
	</div>

	<div class=" col-lg-3">
		<div class="form-group">
			<label for="input3" class="form-label">Barang</label>
			<select class="form-control" name="barang_id" id="input_kn" required>
				<option selected>-- Pilih barang --</option>
				@foreach($barangs as $barang)
					@if($barang->produk_id == 1)
						<option value="{{ $barang->id }}">{{ $barang->nm_barang }}</option>
					@endif
				@endforeach
			</select>
		</div>
	</div>

	<div class=" col-lg-12">
		<div class="form-group">
			<label for="" class="form-label">Finishing</label>
			<div class="row col-md-12">
				<div class="icheck-input">
					<label for="editor_outdoor_label" class="col-md-5">Pilih Finishing</label>
					<input class="i-min-check" class="col-md-1"  onclick="$('#nama_finishing_outdoor').attr('disabled','disabled');$('#editor_outdoor').removeAttr('disabled');" type="radio" id="editor_outdoor_label"  name="finishing" value="old">
					<label for="editor_outdoor" class="col-md-6">
						<select class="form-control" name="editor_id" id="editor_outdoor" disabled="disabled"   required>
							<option selected>-- Pilih Finishing --</option>
							@foreach($editors as $editor)
								@if($editor->produk_id == 1)
									<option value="{{ $editor->id }}">{{ $editor->nama_finishing }}</option>
								@endif
							@endforeach
						</select>
					</label>
				</div>	
				<div class="icheck-input">
					<label for="nama_finishing_outdoor_label" class="col-md-4">Finishing Baru</label>
					<input class="i-min-check" class="col-md-1" onclick="$('#editor_outdoor').attr('disabled','disabled');$('#nama_finishing_outdoor').removeAttr('disabled');" type="radio" id="nama_finishing_outdoor_label"  name="finishing" value="new">
					<label for="nama_finishing_outdoor" class="col-md-7">
						<input type="text" disabled="disabled" class="form-control manual" name="nama_finishing" id="nama_finishing_outdoor" placeholder="Nama Finishing Baru"/>
					</label>
				</div>					
			</div>
		</div>
	</div>

	<div class=" col-lg-4">
		<div class="form-group">
			<label for="" class="form-label">Harga</label>
			<div class="row">
				<div class=" col-lg-6">
					<input type="text" class="form-control" name="harga_pokok" min="0" max="99999999" id="input_kn" placeholder="Pokok" required>
				</div>
				<div class=" col-lg-6">
					<input type="text" class="form-control" name="harga_jual" min="0" max="99999999" id="input_kn" placeholder="Jual" required>
				</div>
			</div>
		</div>
	</div>
	
	<div class=" col-lg-1">
		<div class="form-group">
			<label for="input5" class="form-label">Diskon</label>
			<input type="text" class="form-control" name="disc" id="input_kn" placeholder="%" min="0" value="0" required>
		</div>
	</div>

	<div class=" col-lg-3">
		<div class="form-group">
			<label for="" class="form-label">Range Quantity</label>
			<div class="row">
				<div class=" col-lg-6">
					<input type="number" class="form-control" name="range_min" min="0" id="input_kn" placeholder="Min" required>
				</div>
				<div class=" col-lg-6">
					<input type="number" class="form-control" name="range_max" min="1" id="input_kn" placeholder="Max" required>
				</div>
			</div>
		</div>
	</div>

	<div class=" col-lg-4" style="margin-top: 28px;">
		<button type="submit" class="btn btn-primary">Submit</button>
		<button onclick="goBack()" class="btn btn-default">Kembali</button>
	</div>
</div>
</form>

