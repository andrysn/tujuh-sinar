@extends('layout.layout')
@section('title', 'Tambah Jenis Display')
@section('content')
	<div class="row">
        <div class="col-md-12">
        <div class="panel panel-default">
        	<div class="panel-title"> Tambah Data Jenis Display
        		<ul class="panel-tools">
        			<li><a class="icon minimise-tool"><i class="fa fa-minus"></i></a></li>
        			<li><a class="icon expand-tool"><i class="fa fa-expand"></i></a></li>
        			<li><a class="icon closed-tool"><i class="fa fa-times"></i></a></li>
        		</ul>
        	</div>
        	<div class="panel-body">
        		<form method="post" action="{{ route('display.store') }}">
        			{!! csrf_field() !!}
                    <div class="form-group">
                        <label for="input1" class="form-label">Jenis Display</label>
                        <input type="text" class="form-control" name="jenis_display" id="input1" required>
                    </div>
        			<div class="form-group">
        				<label for="input2" class="form-label">Harga Pokok</label>
        				<input type="text" class="form-control" name="harga_pokok" id="input2" required>
        			</div>
        			<div class="form-group">
        				<label for="input4" class="form-label">Kategori</label>
        				 <select class="form-control" name="kategori" id="input3" required>
        				 	<option value="">Silahkan Pilih</option>
        				 	<option value="meter">Meter</option>
        				 	<option value="lembar">Lembar</option>
        				 </select>
        			</div>
        			<button type="submit" class="btn btn-primary">Submit</button>
                    <button onclick="goBack()" class="btn btn-default">Kembali</button>
        		</form>
        	</div>
        </div>
    </div>
</div>
@endsection