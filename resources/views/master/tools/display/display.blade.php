			<div class="col-md-6">
				@if ( session()->has('alert-display') )
					<div class="foxlabel-alert foxlabel-alert-icon alert3"> <i class="fa fa-check"></i> <a href="#" class="closed">&times;</a> {{ session()->get('alert-display') }}</div>
				@endif
				<div class="panel panel-default">
					<a href="{{ route('display.create') }}" class="btn btn-default"><i class="fa fa-plus-circle"></i>Tambah Data</a>
					<br>
					<br>
					<div class="panel-title"> Data Display </div>
					<div class="panel-body table-responsive">
						<table id="example0" class="table display">
							<thead>
								<tr>
									<th>No</th>
									<th>Jenis Display</th>
									<th>Kategori</th>
									<th>Harga Pokok</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								@forelse($display as $index => $datas)
								<tr>
									<td>{{ $index + 1 }}</td>
									<td>{{ $datas->jenis_display }}</td>
									<td>{{ $datas->kategori }}</td>
									<td>{{ $datas->harga_pokok }}</td>
									<td>
										<form action="{{ route('display.destroy', $datas->id) }}" method="post">
											{{ csrf_field() }}
											{{ method_field('DELETE') }}
										<a href="{{ route('display.edit', $datas->id) }}" class="btn btn-option2"><i class="fa fa-info"></i>Edit</a>
										<button class="btn btn-danger" onclick="return confirm('Yakin ingin menghapus data ?')"><i class="fa fa-check"></i>Delete</button>
										</form>
									</td>
								</tr>
								@empty
								@endforelse
							</tbody>
							<tfoot>

							</tfoot>
						</table>
					</div>
				</div>
			</div>
			<!-- End Panel --> 