@extends('layout.layout')
@section('title', 'Dashboard')
@section('content')

	<div class="container-default animated fadeInRight">
		<div class="row">
			@include('master.tools.barang.barang')
			@include('master.tools.printer.printer')
		</div>

		<div class="row">
			@include('master.tools.potong.potong')
			@include('master.tools.display.display')
		</div>
	</div>
@endsection