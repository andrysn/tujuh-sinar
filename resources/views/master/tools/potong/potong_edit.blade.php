@extends('layout.layout')
@section('title', 'Edit Jenis Potong')
@section('content')
	<div class="row">
        <div class="col-md-12">
        <div class="panel panel-default">
        	<div class="panel-title"> Edit Data Jenis Potong
        		<ul class="panel-tools">
        			<li><a class="icon minimise-tool"><i class="fa fa-minus"></i></a></li>
        			<li><a class="icon expand-tool"><i class="fa fa-expand"></i></a></li>
        			<li><a class="icon closed-tool"><i class="fa fa-times"></i></a></li>
        		</ul>
        	</div>
            @foreach($data as $datas)
        	<div class="panel-body">
        		<form method="post" action="{{ route('potong.update', $datas->id) }}">
        			{!! csrf_field() !!}
                    {{ method_field('PUT') }}
                    <div class="form-group">
                        <label for="input1" class="form-label">Jenis Potong</label>
                        <input type="text" class="form-control" name="jenis_potong" value="{{ $datas->jenis_potong }}" id="input1" required>
                    </div>
        			<div class="form-group">
        				<label for="input2" class="form-label">Harga Pokok</label>
        				<input type="text" class="form-control" name="harga_pokok" value="{{ $datas->harga_pokok }}" id="input2" required>
                    </div>
                    <div class="form-group">
        				<label for="input4" class="form-label">Kategori</label>
        				 <select class="form-control" name="kategori" id="input3" required>
                            <option value="">Silahkan Pilih</option>
                            @if ($datas->kategori == 'meter') 
        				 	    <option value="meter" selected>Meter</option>
                                <option value="lembar">Lembar</option>
                            @elseif ($datas->kategori == 'lembar')
                                <option value="meter">Meter</option>
                                <option value="lembar" selected>Lembar</option>
                            @else
                                <option value="meter">Meter</option>
                                <option value="lembar">Lembar</option>
                            @endif
        				 </select>
        			</div>
        			<button type="submit" class="btn btn-primary">Submit</button>
                    <button onclick="goBack()" class="btn btn-default">Kembali</button>
        		</form>
                @endforeach
        	</div>
        </div>
    </div>
</div>
@endsection