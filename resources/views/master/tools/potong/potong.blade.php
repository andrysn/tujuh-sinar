
			<div class="col-md-6">
				@if ( session()->has('alert-potong') )
					<div class="foxlabel-alert foxlabel-alert-icon alert3"> <i class="fa fa-check"></i> <a href="#" class="closed">&times;</a> {{ session()->get('alert-potong') }}</div>
				@endif
				<div class="panel panel-default">
					<a href="{{ route('potong.create') }}" class="btn btn-default"><i class="fa fa-plus-circle"></i>Tambah Data</a>
					<br>
					<br>
					<div class="panel-title"> Data Potong </div>
					<div class="panel-body table-responsive">
						<table id="example0" class="table display">
							<thead>
								<tr>
									<th>No</th>
									<th>Jenis Potong</th>
									<th>Kategori</th>
									<th>Harga Pokok</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								@forelse($potong as $index => $datas)
								<tr>
									<td>{{ $index + 1 }}</td>
									<td>{{ $datas->jenis_potong }}</td>
									<td>{{ $datas->kategori }}</td>
									<td>{{ $datas->harga_pokok }}</td>
									<td>
										<form action="{{ route('potong.destroy', $datas->id) }}" method="post">
											{{ csrf_field() }}
											{{ method_field('DELETE') }}
										<a href="{{ route('potong.edit', $datas->id) }}" class="btn btn-option2"><i class="fa fa-info"></i>Edit</a>
										<button class="btn btn-danger" onclick="return confirm('Yakin ingin menghapus data ?')"><i class="fa fa-check"></i>Delete</button>
										</form>
									</td>
								</tr>
								@empty
								@endforelse
							</tbody>
							<tfoot>

							</tfoot>
						</table>
					</div>
				</div>
			</div>
			<!-- End Panel --> 