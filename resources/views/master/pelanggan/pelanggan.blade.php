@extends('layout.layout')
@section('title', 'Data Costumer')
@section('content')
	<div class="container-padding animated fadeInRight"> 
		<div class="row"> 
			<!-- Start Panel -->
			<div class="col-md-12">
				@if(Session::has('alert-success'))
					<div class="foxlabel-alert foxlabel-alert-icon alert3"> <i class="fa fa-check"></i> <a href="#" class="closed">&times;</a> {{ \Illuminate\Support\Facades\Session::get('alert-success') }}</div>
				@endif
				<div class="panel panel-default">
					<a href="{{ route('costumer.create') }}" class="btn btn-default"><i class="fa fa-plus-circle"></i>Tambah Data</a>
					<br>
					<br>
					<div class="panel-title"> Data Costumer </div>
					<div class="panel-body table-responsive">
						<table id="example0" class="table display">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama</th>
									<th>Alamat</th>
									<th>No. Telp/HP</th>
									<th>Email</th>
									<th>Tipe Member</th>
									<th>Pesanan Outdoor</th>
									<th>Pesanan Indoor</th>
									<th>Pesanan Print A3</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								@forelse($data as $index => $datas)
								@php($id_pelanggan = $datas->id)
								<tr>
									<td>{{ $index + 1 }}</td>
									<td>{{ $datas->nama }}</td>
									<td>{{ $datas->alamat }}</td>
									<td>{{ $datas->no_telp }}</td>
									<td>{{ $datas->email }}</td>
									<td>{{ $datas->member->nm_tipe }}</td>
									<td>{{ \App\Models\OrderKerjaSub::selectRaw(DB::raw('COUNT(produk_id) AS produk'))
																 ->whereExists(function($que) use ($id_pelanggan){
										  							$que->from('order_kerjas')
																		->whereRaw('order_kerjas.id = order_kerja_subs.order_kerja_id')
																		->where('order_kerja_subs.produk_id', 1)
																		->where('order_kerjas.pelanggan_id', $id_pelanggan); })
																 ->count() }} kali</td>

									<td>{{ \App\Models\OrderKerjaSub::selectRaw(DB::raw('COUNT(produk_id) AS produk'))
																 ->whereExists(function($que) use ($id_pelanggan){
										  							$que->from('order_kerjas')
																		->whereRaw('order_kerjas.id = order_kerja_subs.order_kerja_id')
																		->where('order_kerja_subs.produk_id', 2)
																		->where('order_kerjas.pelanggan_id', $id_pelanggan); })
																 ->count() }} kali</td>

									<td>{{ \App\Models\OrderKerjaSub::selectRaw(DB::raw('COUNT(produk_id) AS produk'))
																 ->whereExists(function($que) use ($id_pelanggan){
										  							$que->from('order_kerjas')
																		->whereRaw('order_kerjas.id = order_kerja_subs.order_kerja_id')
																		->where('order_kerja_subs.produk_id', 4)
																		->where('order_kerjas.pelanggan_id', $id_pelanggan); })
																 ->count() }} kali</td>
									<td>
										<form action="{{ route('costumer.destroy', $datas->id) }}" method="post">
											{{ csrf_field() }}
											{{ method_field('DELETE') }}
										<a href="{{ route('costumer.edit', $datas->id) }}" class="btn btn-option2"><i class="fa fa-info"></i>Edit</a>
										<button class="btn btn-danger" onclick="return confirm('Yakin ingin menghapus data ?')"><i class="fa fa-check"></i>Delete</button>
										</form>
									</td>
								</tr>
								@empty
								@endforelse
							</tbody>
							<tfoot>

							</tfoot>
						</table>
					</div>
				</div>
			</div>
			<!-- End Panel --> 
		</div>
		<!-- End Row --> 
	</div>
@endsection