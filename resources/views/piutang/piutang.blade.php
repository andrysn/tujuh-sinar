@extends('layout.layout')
@section('title', 'Piutang')
@section('content')
	<div class="container-padding animated fadeInRight"> 
		<div class="row"> 
			<!-- Start Panel -->
			<div class="col-md-12">
				@if(Session::has('alert-success'))
					<div class="foxlabel-alert foxlabel-alert-icon alert3"> <i class="fa fa-check"></i> <a href="#" class="closed">&times;</a> {{ \Illuminate\Support\Facades\Session::get('alert-success') }}</div>
				@endif
				<div class="panel panel-default">
					<div class="panel-title"> Data Piutang </div>
					<div class="panel-body table-responsive">
						<table id="example0" class="table display">
							<thead>
								<tr>
									<th>No.</th>
									<th>Tanggal Order</th>
									<th>Pelanggan</th>
									<th style="text-align: right;">Total Harga</th>
									<th>Pembayaran</th>
									<th>Status Order</th>
									<th>Status</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								@forelse($data as $index => $datas)
								<tr>
									<td>{{ $index + 1 }}</td>
									<td>{{ substr($datas->orderkerja->tanggal, 0,10) }}</td>
									<td>{{ $datas->orderkerja->pelanggan->nama }}</td>
									<td align="right">Rp. {{number_format($datas->orderkerja->jumhar)}}</td>
									<td align="center">
										@if($datas->tipe_pembayaran == '')
										Belum ada
										@else
										{{$datas->tipe_pembayaran}}
										@endif
									</td>
									<td align="center">
										@if ($datas->orderkerja->status_produksi == '1')
										Order
										@elseif ($datas->orderkerja->status_produksi == '2')
										Proses
										@elseif ($datas->orderkerja->status_produksi == '3')
										Selesai
										@else
										Sudah diambil
										@endif
									</td>
									<td align="center">{{ $datas->status_pembayaran }}</td>
									<td>
										@if($datas->status_pembayaran == 'Utang')
										<a href="{{ route('piutang.show', $datas->orderkerja->id) }}" class="btn btn-option2"><i class="fa fa-toggle-right"></i>Bayar</a>
										@else
										<a href="{{ route('order.show', $datas->orderkerja->id) }}" class="btn btn-option2"><i class="fa fa-info"></i>Detail</a>
										@endif
									</td>
								</tr>
								@empty
								@endforelse
							</tbody>
							<tfoot>
								
							</tfoot>
						</table>
					</div>
				</div>
			</div>
			<!-- End Panel --> 
		</div>
		<!-- End Row --> 
	</div>
@endsection