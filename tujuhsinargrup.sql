-- Adminer 4.3.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+08:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `antrians`;
CREATE TABLE `antrians` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomor` int(11) NOT NULL,
  `kategori` enum('Cetak','Desain') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `bahans`;
CREATE TABLE `bahans` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `barangs`;
CREATE TABLE `barangs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `supplier_id` int(10) unsigned NOT NULL,
  `barcode` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `produk_id` enum('1','2','3','4','5') COLLATE utf8mb4_unicode_ci NOT NULL,
  `nm_barang` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `satuan` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hrg_beli` double(8,2) NOT NULL,
  `min_stok` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `barangs_supplier_id_index` (`supplier_id`),
  CONSTRAINT `barangs_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `barangs` (`id`, `supplier_id`, `barcode`, `produk_id`, `nm_barang`, `kategori`, `satuan`, `hrg_beli`, `min_stok`, `created_at`, `updated_at`) VALUES
(1,	1,	'981273878112',	'1',	'Kertas HVS A4',	'Kertas',	'Rim',	12000.00,	1,	NULL,	NULL),
(2,	1,	'981264547811',	'1',	'Kertas HVS F4',	'Kertas',	'Rim',	15000.00,	1,	NULL,	NULL),
(3,	2,	'981262347811',	'2',	'Kertas HVS H4',	'Kertas',	'Rim',	54000.00,	1,	NULL,	NULL),
(4,	1,	'31002112',	'3',	'Kertas HVS 80GSM',	'GOODIEBAG',	'pcs',	40000.00,	10,	'2019-03-14 07:12:14',	'2019-03-14 07:12:14'),
(5,	1,	'31002141',	'4',	'Kertas HVS 70GSM',	'GOODIEBAG',	'pcs',	30000.00,	40000,	'2019-03-14 07:27:23',	'2019-03-14 07:27:23');

DROP TABLE IF EXISTS `belis`;
CREATE TABLE `belis` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tgl_pembelian` date NOT NULL,
  `status_pembayaran` enum('Lunas','Utang') COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_harga` double(15,2) NOT NULL,
  `total_bayar` double(15,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `beli_details`;
CREATE TABLE `beli_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `beli_id` int(10) unsigned NOT NULL,
  `barang_id` int(10) unsigned NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` double(15,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `beli_details_beli_id_index` (`beli_id`),
  KEY `beli_details_barang_id_index` (`barang_id`),
  CONSTRAINT `beli_details_barang_id_foreign` FOREIGN KEY (`barang_id`) REFERENCES `barangs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `beli_details_beli_id_foreign` FOREIGN KEY (`beli_id`) REFERENCES `belis` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `editors`;
CREATE TABLE `editors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `produk_id` enum('1','2','3','4','5') COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_finishing` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `editors` (`id`, `produk_id`, `nama_finishing`, `created_at`, `updated_at`) VALUES
(1,	'1',	'Mata ayam',	NULL,	NULL),
(2,	'1',	'Lebih putih',	NULL,	NULL),
(3,	'1',	'Non Mata ayam',	NULL,	NULL),
(4,	'2',	'Laminasi Doft',	NULL,	NULL),
(5,	'2',	'Laminasi Glossy',	NULL,	NULL),
(6,	'2',	'Mata ayam',	NULL,	NULL),
(7,	'2',	'Non Finishing',	NULL,	NULL),
(8,	'4',	'Laminasi Doft',	NULL,	NULL),
(9,	'4',	'Laminasi Glossy',	NULL,	NULL),
(10,	'4',	'Cutting',	NULL,	NULL),
(11,	'4',	'Laminasi + Cut Doft',	NULL,	NULL),
(12,	'4',	'Laminasi + Cut Glossy',	NULL,	NULL),
(13,	'4',	'Non Finishing',	NULL,	NULL);

DROP TABLE IF EXISTS `finishings`;
CREATE TABLE `finishings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `hargas`;
CREATE TABLE `hargas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL,
  `produk_id` enum('1','2','3','4','5') COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `hargas_member_id_index` (`member_id`),
  CONSTRAINT `hargas_member_id_foreign` FOREIGN KEY (`member_id`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `hargas` (`id`, `member_id`, `produk_id`, `keterangan`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	1,	'1',	'test',	'2019-03-14 06:45:12',	'2019-03-14 06:45:12',	NULL),
(2,	2,	'2',	'test',	'2019-03-14 06:46:38',	'2019-03-14 06:46:38',	NULL),
(3,	2,	'1',	'test',	'2019-03-14 06:49:36',	'2019-03-14 06:50:00',	'2019-03-14 06:50:00'),
(4,	3,	'3',	'komunitas',	'2019-03-14 07:12:51',	'2019-03-14 07:12:51',	NULL),
(5,	2,	'4',	'komunitas',	'2019-03-14 07:28:10',	'2019-03-14 07:28:10',	NULL),
(6,	1,	'2',	'test umum',	'2019-03-15 07:18:19',	'2019-03-15 07:18:19',	NULL);

DROP TABLE IF EXISTS `harga_costums`;
CREATE TABLE `harga_costums` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `harga_id` int(10) unsigned NOT NULL,
  `nama_produk` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `range_min` int(11) NOT NULL,
  `range_max` int(11) NOT NULL,
  `harga_pokok` double(8,2) NOT NULL,
  `harga_jual` double(8,2) NOT NULL,
  `disc` int(11) NOT NULL,
  `keterangan` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `harga_costums_harga_id_index` (`harga_id`),
  CONSTRAINT `harga_costums_harga_id_foreign` FOREIGN KEY (`harga_id`) REFERENCES `hargas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `harga_indoors`;
CREATE TABLE `harga_indoors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `harga_id` int(10) unsigned NOT NULL,
  `editor_id` int(10) unsigned NOT NULL,
  `barang_id` int(10) unsigned NOT NULL,
  `tipe_print` enum('1','2','3') COLLATE utf8mb4_unicode_ci NOT NULL,
  `range_min` int(11) NOT NULL,
  `range_max` int(11) NOT NULL,
  `harga_pokok` double(8,2) NOT NULL,
  `harga_jual` double(8,2) NOT NULL,
  `disc` int(11) NOT NULL,
  `keterangan` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `harga_flyers_harga_id_index` (`harga_id`),
  KEY `harga_flyers_editor_id_index` (`editor_id`),
  KEY `harga_flyers_barang_id_index` (`barang_id`),
  CONSTRAINT `harga_flyers_barang_id_foreign` FOREIGN KEY (`barang_id`) REFERENCES `barangs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `harga_flyers_editor_id_foreign` FOREIGN KEY (`editor_id`) REFERENCES `editors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `harga_flyers_harga_id_foreign` FOREIGN KEY (`harga_id`) REFERENCES `hargas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `harga_indoors` (`id`, `harga_id`, `editor_id`, `barang_id`, `tipe_print`, `range_min`, `range_max`, `harga_pokok`, `harga_jual`, `disc`, `keterangan`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	2,	5,	3,	'2',	1,	30,	30000.00,	34000.00,	0,	NULL,	'2019-03-14 06:46:38',	'2019-03-14 07:08:40',	'2019-03-14 07:08:40'),
(2,	2,	5,	3,	'1',	300,	2000,	40000.00,	60000.00,	0,	NULL,	'2019-03-14 06:57:16',	'2019-03-14 06:57:16',	NULL),
(3,	6,	6,	3,	'3',	1,	50,	40000.00,	60000.00,	0,	NULL,	'2019-03-15 07:18:19',	'2019-03-15 07:18:19',	NULL);

DROP TABLE IF EXISTS `harga_larges`;
CREATE TABLE `harga_larges` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `harga_id` int(10) unsigned NOT NULL,
  `printer_id` int(10) unsigned NOT NULL,
  `barang_id` int(10) unsigned NOT NULL,
  `cutting` enum('0','1','2') COLLATE utf8mb4_unicode_ci NOT NULL,
  `laminating` enum('0','1','2','3') COLLATE utf8mb4_unicode_ci NOT NULL,
  `panjang` double(5,2) NOT NULL,
  `lebar` double(5,2) NOT NULL,
  `range_min` int(11) NOT NULL,
  `range_max` int(11) NOT NULL,
  `harga_pokok` double(8,2) NOT NULL,
  `harga_jual` double(8,2) NOT NULL,
  `disc` int(11) NOT NULL,
  `keterangan` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `harga_larges_harga_id_index` (`harga_id`),
  KEY `harga_larges_printer_id_index` (`printer_id`),
  KEY `harga_larges_barang_id_index` (`barang_id`),
  CONSTRAINT `harga_larges_barang_id_foreign` FOREIGN KEY (`barang_id`) REFERENCES `barangs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `harga_larges_harga_id_foreign` FOREIGN KEY (`harga_id`) REFERENCES `hargas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `harga_larges_printer_id_foreign` FOREIGN KEY (`printer_id`) REFERENCES `printers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `harga_merchants`;
CREATE TABLE `harga_merchants` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `harga_id` int(10) unsigned NOT NULL,
  `barang_id` int(10) unsigned NOT NULL,
  `range_min` int(11) NOT NULL,
  `range_max` int(11) NOT NULL,
  `harga_pokok` double(8,2) NOT NULL,
  `harga_jual` double(8,2) NOT NULL,
  `disc` int(11) NOT NULL,
  `keterangan` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `harga_displays_harga_id_index` (`harga_id`),
  KEY `harga_displays_barang_id_index` (`barang_id`),
  CONSTRAINT `harga_displays_barang_id_foreign` FOREIGN KEY (`barang_id`) REFERENCES `barangs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `harga_displays_harga_id_foreign` FOREIGN KEY (`harga_id`) REFERENCES `hargas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `harga_merchants` (`id`, `harga_id`, `barang_id`, `range_min`, `range_max`, `harga_pokok`, `harga_jual`, `disc`, `keterangan`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	4,	4,	10,	30,	40000.00,	44000.00,	0,	NULL,	'2019-03-14 07:12:51',	'2019-03-14 07:12:51',	NULL);

DROP TABLE IF EXISTS `harga_outdoors`;
CREATE TABLE `harga_outdoors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `harga_id` int(10) unsigned NOT NULL,
  `editor_id` int(10) unsigned NOT NULL,
  `barang_id` int(10) unsigned NOT NULL,
  `range_min` int(11) NOT NULL,
  `range_max` int(11) NOT NULL,
  `harga_pokok` double(8,2) NOT NULL,
  `harga_jual` double(8,2) NOT NULL,
  `disc` int(11) NOT NULL,
  `keterangan` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `harga_outdoors_harga_id_index` (`harga_id`),
  KEY `harga_outdoors_editor_id_index` (`editor_id`),
  KEY `harga_outdoors_barang_id_index` (`barang_id`),
  CONSTRAINT `harga_outdoors_barang_id_foreign` FOREIGN KEY (`barang_id`) REFERENCES `barangs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `harga_outdoors_editor_id_foreign` FOREIGN KEY (`editor_id`) REFERENCES `editors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `harga_outdoors_harga_id_foreign` FOREIGN KEY (`harga_id`) REFERENCES `hargas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `harga_outdoors` (`id`, `harga_id`, `editor_id`, `barang_id`, `range_min`, `range_max`, `harga_pokok`, `harga_jual`, `disc`, `keterangan`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	1,	2,	2,	1,	20,	20000.00,	31000.00,	0,	NULL,	'2019-03-14 06:45:13',	'2019-03-14 06:45:56',	'2019-03-14 06:45:56'),
(2,	1,	3,	2,	1,	30,	30000.00,	31000.00,	0,	NULL,	'2019-03-14 06:45:48',	'2019-03-14 06:45:48',	NULL),
(3,	3,	1,	1,	10,	100,	30000.00,	32000.00,	3,	NULL,	'2019-03-14 06:49:36',	'2019-03-14 06:49:36',	NULL);

DROP TABLE IF EXISTS `harga_prints`;
CREATE TABLE `harga_prints` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `harga_id` int(10) unsigned NOT NULL,
  `editor_id` int(10) unsigned NOT NULL,
  `barang_id` int(10) unsigned NOT NULL,
  `tipe_print` enum('1','2','3') COLLATE utf8mb4_unicode_ci NOT NULL,
  `kaki` enum('0','1','2','3','4','5') COLLATE utf8mb4_unicode_ci NOT NULL,
  `range_min` int(11) NOT NULL,
  `range_max` int(11) NOT NULL,
  `harga_pokok` double(8,2) NOT NULL,
  `harga_jual` double(8,2) NOT NULL,
  `disc` int(11) NOT NULL,
  `keterangan` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `harga_prints_harga_id_index` (`harga_id`),
  KEY `harga_prints_editor_id_index` (`editor_id`),
  KEY `harga_prints_barang_id_index` (`barang_id`),
  CONSTRAINT `harga_prints_barang_id_foreign` FOREIGN KEY (`barang_id`) REFERENCES `barangs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `harga_prints_editor_id_foreign` FOREIGN KEY (`editor_id`) REFERENCES `editors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `harga_prints_harga_id_foreign` FOREIGN KEY (`harga_id`) REFERENCES `hargas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `harga_prints` (`id`, `harga_id`, `editor_id`, `barang_id`, `tipe_print`, `kaki`, `range_min`, `range_max`, `harga_pokok`, `harga_jual`, `disc`, `keterangan`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	5,	12,	5,	'2',	'3',	31,	3100,	40000.00,	52000.00,	0,	NULL,	'2019-03-14 07:28:11',	'2019-03-14 07:28:11',	NULL);

DROP TABLE IF EXISTS `jenis_displays`;
CREATE TABLE `jenis_displays` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jenis_potong` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori` enum('meter','lembar') COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga_pokok` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `jenis_potongs`;
CREATE TABLE `jenis_potongs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jenis_potong` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori` enum('meter','lembar') COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga_pokok` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `members`;
CREATE TABLE `members` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nm_tipe` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `members` (`id`, `nm_tipe`, `keterangan`, `created_at`, `updated_at`) VALUES
(1,	'Umum',	'Member untuk umum',	NULL,	NULL),
(2,	'Perusahaan',	'Member untuk perusahaan',	NULL,	NULL),
(3,	'Komunitas',	'Member untuk komunitas',	NULL,	NULL);

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1,	'2014_10_12_000000_create_users_table',	1),
(2,	'2014_10_12_100000_create_password_resets_table',	1),
(3,	'2018_08_23_085456_create_logins_table',	1),
(4,	'2018_08_25_135350_create_bahans_table',	1),
(5,	'2018_08_26_102611_create_printer_table',	1),
(6,	'2018_08_26_102712_create_supplier_table',	1),
(7,	'2018_08_26_102803_create_member_table',	1),
(8,	'2018_08_26_102813_create_pelanggan_table',	1),
(9,	'2018_08_26_102816_create_barang_table',	1),
(10,	'2018_08_26_102830_create_editor_table',	1),
(11,	'2018_08_26_102951_create_harga_table',	1),
(12,	'2018_08_26_103012_create_harga_prints_table',	1),
(13,	'2018_08_26_103023_create_harga_larges_table',	1),
(14,	'2018_08_26_103041_create_harga_costums_table',	1),
(15,	'2018_08_26_103044_create_harga_flyers_table',	1),
(16,	'2018_08_26_103056_create_harga_displays_table',	1),
(17,	'2018_08_26_103056_create_harga_kartu_namas_table',	1),
(18,	'2018_08_26_132636_create_jenis_potongs_table',	1),
(19,	'2018_08_26_132901_create_jenis_displays_table',	1),
(20,	'2018_09_12_080959_create_antrians_table',	1),
(21,	'2018_09_15_025853_create_order_kerjas_table',	1),
(22,	'2018_09_15_030436_create_order_kerja_subs_table',	1),
(23,	'2018_09_25_040023_create_produksis_table',	1),
(24,	'2018_10_02_022111_create_pembelians_table',	1),
(25,	'2018_10_03_111111_create_stok_barang_pembelians_table',	1),
(26,	'2018_10_03_124116_create_stok_barangs_table',	1),
(27,	'2018_11_26_144028_create_belis_table',	1),
(28,	'2018_11_26_145050_create_beli_details_table',	1),
(29,	'2018_11_27_141355_create_tmp_belis_table',	1),
(30,	'2018_12_10_142048_create_piutangs_table',	1),
(31,	'2019_02_06_195821_create_finishings_table',	1),
(32,	'2019_02_27_180727_create_pengambils_table',	1);

DROP TABLE IF EXISTS `order_kerjas`;
CREATE TABLE `order_kerjas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` datetime NOT NULL,
  `pelanggan_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_kerjas_pelanggan_id_index` (`pelanggan_id`),
  CONSTRAINT `order_kerjas_pelanggan_id_foreign` FOREIGN KEY (`pelanggan_id`) REFERENCES `pelanggans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `order_kerjas` (`id`, `order`, `tanggal`, `pelanggan_id`, `created_at`, `updated_at`) VALUES
(1,	'00001',	'2019-03-15 00:00:00',	1,	'2019-03-15 07:11:40',	'2019-03-18 06:27:56'),
(2,	'00002',	'2019-03-20 00:00:00',	1,	'2019-03-20 06:15:40',	'2019-03-20 06:15:40');

DROP TABLE IF EXISTS `order_kerja_subs`;
CREATE TABLE `order_kerja_subs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_kerja_id` int(10) unsigned NOT NULL,
  `deadline` datetime NOT NULL,
  `barang_id` int(10) unsigned DEFAULT NULL,
  `produk_id` enum('1','2','3','4','5') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_produksi` enum('1','2','3') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `harga` double(11,2) NOT NULL,
  `total` double(11,2) NOT NULL,
  `diskon` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_kerja_subs_order_kerja_id_index` (`order_kerja_id`),
  KEY `order_kerja_subs_barang_id_index` (`barang_id`),
  CONSTRAINT `order_kerja_subs_barang_id_foreign` FOREIGN KEY (`barang_id`) REFERENCES `barangs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `order_kerja_subs_order_kerja_id_foreign` FOREIGN KEY (`order_kerja_id`) REFERENCES `order_kerjas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `order_kerja_subs` (`id`, `order_kerja_id`, `deadline`, `barang_id`, `produk_id`, `status_produksi`, `harga`, `total`, `diskon`, `qty`, `keterangan`, `created_at`, `updated_at`) VALUES
(1,	1,	'2019-03-17 00:00:00',	2,	'1',	'3',	31000.00,	620000.00,	0,	20,	'Nama File: kembali lah wahay doni.exe<br>Ukuran: 1.00x1.00<br>Finishing: Non Mata ayam<br>Kaki: <br>',	'2019-03-15 07:11:40',	'2019-03-19 08:48:42'),
(2,	1,	'2019-03-16 00:00:00',	2,	'1',	'2',	31000.00,	620000.00,	0,	10,	'Nama File: kembali lah wahay doni 2.exe<br>Ukuran: 2.00x1.00<br>Finishing: Non Mata ayam<br>Kaki: <br>',	'2019-03-15 07:12:42',	'2019-03-19 08:45:17'),
(3,	1,	'2019-03-16 00:00:00',	2,	'1',	'3',	31000.00,	1240000.00,	0,	20,	'Nama File: kembali lah wahay doni<br>Ukuran: 2.00x1.00<br>Finishing: Non Mata ayam<br>Kaki: <br>',	'2019-03-15 07:14:49',	'2019-03-19 08:48:48'),
(4,	1,	'2019-03-16 00:00:00',	3,	'2',	'2',	60000.00,	960000.00,	0,	2,	'Nama File: kembali lah wahay doni 4<br>Ukuran: 4.00x2.00<br>Finishing: Mata ayam<br>Kaki: <br>Tipe Print : Cut Only<br>',	'2019-03-15 07:22:08',	'2019-03-20 06:19:14'),
(5,	2,	'2019-03-21 00:00:00',	2,	'1',	'1',	31000.00,	1860000.00,	0,	20,	'Nama File: priyo kapan pulang.dll<br>Ukuran: 3.00x1.00<br>Finishing: Non Mata ayam<br>Kaki: <br>',	'2019-03-20 06:15:41',	'2019-03-20 06:15:41'),
(6,	2,	'2019-03-21 00:00:00',	3,	'2',	'3',	60000.00,	600000.00,	0,	5,	'Nama File: priyo kapan pulang.batch<br>Ukuran: 1.00x2.00<br>Finishing: Mata ayam<br>Kaki: <br>Tipe Print : Cut Only<br>',	'2019-03-20 06:17:25',	'2019-03-20 06:18:37');

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `pelanggans`;
CREATE TABLE `pelanggans` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL,
  `nama` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pelanggans_member_id_index` (`member_id`),
  CONSTRAINT `pelanggans_member_id_foreign` FOREIGN KEY (`member_id`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `pelanggans` (`id`, `member_id`, `nama`, `alamat`, `no_telp`, `email`, `created_at`, `updated_at`) VALUES
(1,	1,	'Doni Ahmad',	'Jl. Lambung Mangkurat Gg.3 Blok B',	'085245762133',	'donyahmd24@gmail.com',	NULL,	NULL),
(2,	2,	'Yusril Mahendra',	'Jl. Lambung Mangkurat Gg.3',	'085234322433',	'mahendrayusril@gmail.com',	NULL,	NULL);

DROP TABLE IF EXISTS `pembelians`;
CREATE TABLE `pembelians` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tgl_pembelian` date NOT NULL,
  `tipe_pembayaran` enum('Tunai','Kredit') COLLATE utf8mb4_unicode_ci NOT NULL,
  `supplier_id` int(10) unsigned NOT NULL,
  `barang_id` int(10) unsigned NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pembelians_supplier_id_index` (`supplier_id`),
  KEY `pembelians_barang_id_index` (`barang_id`),
  CONSTRAINT `pembelians_barang_id_foreign` FOREIGN KEY (`barang_id`) REFERENCES `barangs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pembelians_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `pengambils`;
CREATE TABLE `pengambils` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `piutangs`;
CREATE TABLE `piutangs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_kerja_id` int(10) unsigned NOT NULL,
  `sudah_bayar` double(15,2) NOT NULL,
  `tipe_pembayaran` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ket_piutang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_pembayaran` enum('Lunas','Utang','Cancel') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `piutangs_order_kerja_id_index` (`order_kerja_id`),
  CONSTRAINT `piutangs_order_kerja_id_foreign` FOREIGN KEY (`order_kerja_id`) REFERENCES `order_kerjas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `piutangs` (`id`, `order_kerja_id`, `sudah_bayar`, `tipe_pembayaran`, `ket_piutang`, `status_pembayaran`, `created_at`, `updated_at`) VALUES
(1,	1,	0.00,	NULL,	NULL,	'Utang',	'2019-03-15 07:11:40',	'2019-03-15 07:22:08'),
(2,	2,	0.00,	NULL,	NULL,	'Utang',	'2019-03-20 06:15:41',	'2019-03-20 06:17:25');

DROP TABLE IF EXISTS `printers`;
CREATE TABLE `printers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kd_printer` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nm_printer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `merk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('ada','digunakan','rusak') COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `printers` (`id`, `kd_printer`, `nm_printer`, `merk`, `status`, `keterangan`, `created_at`, `updated_at`) VALUES
(1,	'C-8001',	'Xerox D-95 B/W',	'Xerox',	'ada',	'ini keterangan printer',	NULL,	NULL),
(2,	'D-0012',	'Xerox D-10 Full',	'Xerox',	'ada',	'ini keterangan printer',	NULL,	NULL);

DROP TABLE IF EXISTS `produksis`;
CREATE TABLE `produksis` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `stok_barangs`;
CREATE TABLE `stok_barangs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `stok_barang_pembelian_id` int(10) unsigned NOT NULL,
  `tgl` date NOT NULL,
  `qty_keluar` int(11) NOT NULL,
  `keperluan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `stok_barangs_stok_barang_pembelian_id_index` (`stok_barang_pembelian_id`),
  CONSTRAINT `stok_barangs_stok_barang_pembelian_id_foreign` FOREIGN KEY (`stok_barang_pembelian_id`) REFERENCES `stok_barang_pembelians` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `stok_barang_pembelians`;
CREATE TABLE `stok_barang_pembelians` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pembelian_id` int(10) unsigned NOT NULL,
  `qty` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `stok_barang_pembelians_pembelian_id_index` (`pembelian_id`),
  CONSTRAINT `stok_barang_pembelians_pembelian_id_foreign` FOREIGN KEY (`pembelian_id`) REFERENCES `pembelians` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `suppliers`;
CREATE TABLE `suppliers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nm_lengkap` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `suppliers` (`id`, `nm_lengkap`, `alamat`, `no_telp`, `email`, `keterangan`, `created_at`, `updated_at`) VALUES
(1,	'CV. GreenNusa Computindo',	'Jl. Depan Lembus',	'085283782723',	'donyahmd@gmail.com',	'keterangan dawdawaw',	NULL,	NULL),
(2,	'CV. GreenNusa Computindo 2',	'Jl. Depan Lembus',	'0852352355',	'dwadwad@gmail.com',	'wadiijlk 72878',	NULL,	NULL);

DROP TABLE IF EXISTS `tmp_belis`;
CREATE TABLE `tmp_belis` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `barang_id` int(10) unsigned NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` double(15,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tmp_belis_barang_id_index` (`barang_id`),
  CONSTRAINT `tmp_belis_barang_id_foreign` FOREIGN KEY (`barang_id`) REFERENCES `barangs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `users` (`id`, `nama`, `alamat`, `no_telp`, `username`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1,	'admin.id',	'Lambung Mangkurat',	'0836367333',	'admin',	'$2y$10$8SZTOXjZjIIwqDB4L/Q.kOiERrheHctF809U2AP/4Cd0mk6ySc1iy',	1,	NULL,	NULL,	NULL),
(2,	'petugas1.id',	'Lambung Mangkurat',	'0836367333',	'petugas1',	'$2y$10$ARd.LeGPzX6yunIXO6tnr.dfEZu1Og1ClWUEnjqzHDmshGq0p4jy2',	1,	NULL,	NULL,	NULL);

-- 2019-03-20 08:18:57