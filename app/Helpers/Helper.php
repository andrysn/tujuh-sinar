<?php

namespace App\Helpers;

use App\Models\User;
use Illuminate\Support\Facades\DB;

class Helper
{
    /**
     * Creating helpers for getUsername.
     *
     * @return \Illuminate\Support\Facades
     */
    public static function get_username($user_id)
    {
        $user = User::where('id', $user_id)->first();
        return (isset($user->username) ? $user->username : '');
    }

}