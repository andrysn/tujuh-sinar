<?php

namespace App\Http\Controllers;

use Session;

use App\Models\JenisDisplay;
use Illuminate\Http\Request;

class JenisDisplayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = JenisDisplay::all();
        return view('master.tools.display.display', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('master.tools.display.display_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new JenisDisplay();
        $data->jenis_display = $request->jenis_display;
        $data->kategori = $request->kategori;
        $data->harga_pokok = $request->harga_pokok;
        $data->save();
        return redirect()->route('tools.index')->with('alert-display', 'Data Berhasil Ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\JenisDisplay  $jenisDisplay
     * @return \Illuminate\Http\Response
     */
    public function show(JenisDisplay $jenisDisplay)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\JenisDisplay  $jenisDisplay
     * @return \Illuminate\Http\Response
     */
    public function edit(JenisDisplay $id)
    {
        $data = JenisDisplay::where('id', $id)->get();
        return view('master.tools.display.display_edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\JenisDisplay  $jenisDisplay
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JenisDisplay $id)
    {
        $data = JenisDisplay::where('id', $id)->first();
        $data->jenis_display = $request->jenis_display;
        $data->harga_pokok = $request->harga_pokok;
        $data->kategori = $request->kategori;
        $data->save();
        return redirect()->route('tools.index')->with('alert-display', 'Data Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\JenisDisplay  $jenisDisplay
     * @return \Illuminate\Http\Response
     */
    public function destroy(JenisDisplay $id)
    {
        $data = JenisDisplay::where('id', $id)->first();
        $data->delete();
        return redirect()->route('tools.index')->with('alert-display','Data berhasi dihapus!');
    }
}
