<?php

namespace App\Http\Controllers;

use App\Models\Harga;
use App\Models\Editor;
use App\Models\Barang;
use App\Models\Member;
use App\Models\Printer;
use App\Models\Piutang;
use App\Models\Pelanggan;
use App\Models\OrderKerja;
use App\Models\OrderKerjaSub;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderKerjaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = OrderKerja::orderBy('order', 'desc')->get();
        return view('transaksi.order.order', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cari = OrderKerja::count();
        if($cari == 0 ) {
            $add = 1;
        } else {
            $add = $cari + 1;
        }
        $kode = sprintf("%05s", $add);

        $member = Member::all();
        $data = Harga::all();
        $barangs = Barang::all();
        $printers = Printer::all();
        $members = Pelanggan::all();
        $order = OrderKerja::latest('id')->first();
        $editors = Editor::all();
        return view('transaksi.order.order_create', compact(['members', 'printers', 'barangs', 'kode', 'member', 'order','editors']));
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeOutdoor(Request $request)
    {
        $cekData = OrderKerja::where('order', $request->order)->first();

        if ( is_null($cekData) ) {
            $orderKerjaBaru = new OrderKerja;
            $orderKerjaBaru->order = $request->order;
            $orderKerjaBaru->tanggal = date('Y-m-d');
            $orderKerjaBaru->pelanggan_id = $request->pelanggan_id;
            $orderKerjaBaru->save();
            $orderKerjaId = $orderKerjaBaru->id;
            $this->piut($orderKerjaBaru->id);
        } else {
            $orderKerjaId = $cekData->id;
            $this->anu($orderKerjaId);
        }
        $deadline = '';
        switch ($request->deadline) {
            case 'besok':
                $deadline = date('Y-m-d', strtotime('+1 day'));
                break;
            case 'lusa':
                $deadline = date('Y-m-d', strtotime('+2 day'));
                break;
            case 'pilih':
                $deadline = $request->pilih_deadline_outdoor;
                break;
            default:
                $deadline =  date('Y-m-d');
                break;
        }
        $subOrderKerjaBaru = new OrderKerjaSub;
        $subOrderKerjaBaru->order_kerja_id = $orderKerjaId;
        $subOrderKerjaBaru->produk_id = $request->produk_id;
        $subOrderKerjaBaru->qty = $request->qty;
        $subOrderKerjaBaru->deadline = $deadline;
        $subOrderKerjaBaru->harga = $request->harga;
        $subOrderKerjaBaru->total = $request->total;
        $subOrderKerjaBaru->diskon = $request->diskon;
        $subOrderKerjaBaru->barang_id = $request->barang_id;
        $subOrderKerjaBaru->keterangan  = 'Nama File: '.$request->nama_file."<br>";
        $subOrderKerjaBaru->keterangan  .= 'Ukuran: '.$request->panjang."x".$request->lebar."<br>";
        $ed = Editor::find($request->editor_id);
        $subOrderKerjaBaru->keterangan  .= 'Finishing: '.$ed->nama_finishing."<br>";

        $kaki = '';
        switch ($request->kaki) {
            case '1':
                $kaki = "X-Banner";
                break;
            case '2':
                $kaki = "Roll Banner";
                break;
            case '3':
                $kaki = "Y Banner";
                break;
            case '3':
                $kaki = "Rollup";
                break;
            case '4':
                $kaki = "Toneup";
                break;
            default:
                $kaki = "";
                break;
        }
        $subOrderKerjaBaru->keterangan  .= 'Kaki: '.$kaki."<br>";
        
        $subOrderKerjaBaru->save();


        return redirect()->route('order.show', $orderKerjaId);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeIndoor(Request $request)
    {
        $cekData = OrderKerja::where('order', $request->order)->first();
        
        if ( is_null($cekData) ) {
            $orderKerjaBaru = new OrderKerja;
            $orderKerjaBaru->order = $request->order;
            $orderKerjaBaru->tanggal = date('Y-m-d');
            $orderKerjaBaru->pelanggan_id = $request->pelanggan_id;
            $orderKerjaBaru->save();
            $orderKerjaId = $orderKerjaBaru->id;
            $this->piut($orderKerjaBaru->id);
        } else {
            $orderKerjaId = $cekData->id;
            $this->anu($orderKerjaId);
        }
        $deadline = '';
        switch ($request->deadline_indoor) {
            case 'besok':
                $deadline = date('Y-m-d', strtotime('+1 day'));
                break;
            case 'lusa':
                $deadline = date('Y-m-d', strtotime('+2 day'));
                break;
            case 'pilih':
                $deadline = $request->pilih_deadline_outdoor;
                break;
            default:
                $deadline =  date('Y-m-d');
                break;
        }

        $tipe_print = '';
        switch ($request->tipe_print) {
            case '1':
                $tipe_print = 'Print Only';
                break;
            case '2':
                $tipe_print = 'Print + Cut';
                break;
            case '3':
                $tipe_print = 'Cut Only';
                break;
            default:
                $deadline =  date('Y-m-d');
                break;
        }
        $subOrderKerjaBaru = new OrderKerjaSub;
        $subOrderKerjaBaru->order_kerja_id = $orderKerjaId;
        $subOrderKerjaBaru->produk_id = $request->produk_id;
        $subOrderKerjaBaru->qty = $request->qty;
        $subOrderKerjaBaru->deadline = $deadline;
        $subOrderKerjaBaru->harga = $request->harga;
        $subOrderKerjaBaru->total = $request->total;
        $subOrderKerjaBaru->diskon = $request->diskon;
        $subOrderKerjaBaru->barang_id = $request->barang_id;
        $subOrderKerjaBaru->keterangan  = 'Nama File: '.$request->nama_file."<br>";
        $subOrderKerjaBaru->keterangan  .= 'Ukuran: '.$request->panjang."x".$request->lebar."<br>";
        $ed = Editor::find($request->editor_id);
        $subOrderKerjaBaru->keterangan  .= 'Finishing: '.$ed->nama_finishing."<br>";

        $kaki = '';
        switch ($request->kaki) {
            case '1':
                $kaki = "X-Banner";
                break;
            case '2':
                $kaki = "Roll Banner";
                break;
            case '3':
                $kaki = "Y Banner";
                break;
            case '3':
                $kaki = "Rollup";
                break;
            case '4':
                $kaki = "Toneup";
                break;
            default:
                $kaki = "";
                break;
        }
        $subOrderKerjaBaru->keterangan  .= 'Kaki: '.$kaki."<br>";
        $subOrderKerjaBaru->keterangan  .= 'Tipe Print : '.$tipe_print."<br>";
        
        $subOrderKerjaBaru->save();


        return redirect()->route('order.show', $orderKerjaId);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeMerchant(Request $request)
    {
        $cekData = OrderKerja::where('order', $request->order)->first();
        
        if ( is_null($cekData) ) {
            $orderKerjaBaru = new OrderKerja;
            $orderKerjaBaru->order = $request->order;
            $orderKerjaBaru->tanggal = date('Y-m-d');
            $orderKerjaBaru->pelanggan_id = $request->pelanggan_id;
            
            $orderKerjaBaru->save();
            $this->piut($orderKerjaBaru->id);
            $orderKerjaId = $orderKerjaBaru->id;
        } else {
            $orderKerjaId = $cekData->id;
            $this->anu($orderKerjaId);
        }
        $deadline = '';
        switch ($request->deadline_custom) {
            case 'besok':
                $deadline = date('Y-m-d', strtotime('+1 day'));
                break;
            case 'lusa':
                $deadline = date('Y-m-d', strtotime('+2 day'));
                break;
            case 'pilih':
                $deadline = $request->pilih_deadline_outdoor;
                break;
            default:
                $deadline =  date('Y-m-d');
                break;
        }
        $subOrderKerjaBaru = new OrderKerjaSub;
        $subOrderKerjaBaru->order_kerja_id = $orderKerjaId;
        $subOrderKerjaBaru->produk_id = $request->produk_id;
        $subOrderKerjaBaru->qty = $request->qty;
        $subOrderKerjaBaru->harga = $request->harga;
        $subOrderKerjaBaru->total = $request->total;
        $subOrderKerjaBaru->diskon = $request->diskon;
        $subOrderKerjaBaru->deadline = $deadline;
        $subOrderKerjaBaru->barang_id = $request->barang_id;
        $subOrderKerjaBaru->keterangan  = 'Merchant';
        $subOrderKerjaBaru->save();


        return redirect()->route('order.show', $orderKerjaId);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storePrintQuarto(Request $request)
    {
        $cekData = OrderKerja::where('order', $request->order)->first();
        
        if ( is_null($cekData) ) {
            $orderKerjaBaru = new OrderKerja;
            $orderKerjaBaru->order = $request->order;
            $orderKerjaBaru->tanggal = date('Y-m-d');
            $orderKerjaBaru->pelanggan_id = $request->pelanggan_id;
            $orderKerjaBaru->save();
            $orderKerjaId = $orderKerjaBaru->id;
            $this->piut($orderKerjaBaru->id);
        } else {
            $orderKerjaId = $cekData->id;
            $this->anu($orderKerjaId);
        }

        $deadline = '';
        switch ($request->deadline_print) {
            case 'besok':
                $deadline = date('Y-m-d', strtotime('+1 day'));
                break;
            case 'lusa':
                $deadline = date('Y-m-d', strtotime('+2 day'));
                break;
            case 'pilih':
                $deadline = $request->pilih_deadline_print;
                break;
            default:
                $deadline =  date('Y-m-d');
                break;
        }

        $ukuran = '';
        if($request->ukuran == 'pilih'){
            switch ($request->pilih_ukuran) {
                case '1':
                    $ukuran = 'A4';
                    break;
                case '2':
                    $ukuran = 'F4';
                    break;
                case '3':
                    $ukuran = 'A3';
                    break;
                default:
                    $deadline =  date('Y-m-d');
                    break;
            }
        } else if($request->ukuran =='input'){
            $ukuran = $request->panjang.' X '.$request->lebar;
        }
            

        $kaki = '';
        switch ($request->kaki) {
            case '1':
                $kaki = "X-Banner";
                break;
            case '2':
                $kaki = "Roll Banner";
                break;
            case '3':
                $kaki = "Y Banner";
                break;
            case '3':
                $kaki = "Rollup";
                break;
            case '4':
                $kaki = "Toneup";
                break;
            default:
                $kaki = "";
                break;
        }

        $subOrderKerjaBaru = new OrderKerjaSub;
        $subOrderKerjaBaru->order_kerja_id = $orderKerjaId;
        $subOrderKerjaBaru->produk_id = $request->produk_id;
        $subOrderKerjaBaru->qty = $request->qty;
        $subOrderKerjaBaru->harga = $request->harga;
        $subOrderKerjaBaru->total = $request->total;
        $subOrderKerjaBaru->diskon = $request->diskon;
        $subOrderKerjaBaru->deadline = $deadline;
        $subOrderKerjaBaru->barang_id = $request->barang_id;
        $subOrderKerjaBaru->keterangan  = 'Ukuran : '.$ukuran."<br>";
        $ed = Editor::find($request->editor_id);
        $subOrderKerjaBaru->keterangan  .= 'Finishing: '.$ed->nama_finishing."<br>";
        $subOrderKerjaBaru->keterangan  .= 'Kaki: '.$kaki."<br>";
        $subOrderKerjaBaru->save();


        return redirect()->route('order.show', $orderKerjaId);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeCostumProduk(Request $request) 
    {
        $cekData = OrderKerja::where('order', $request->order)->first();
        
        if ( is_null($cekData) ) {
            $orderKerjaBaru = new OrderKerja;
            $orderKerjaBaru->order = $request->order;
            $orderKerjaBaru->tanggal =date('Y-m-d');
            $orderKerjaBaru->pelanggan_id = $request->pelanggan_id;
            $orderKerjaBaru->save();
            $orderKerjaId = $orderKerjaBaru->id;
            $this->piut($orderKerjaBaru->id);
        } else {
            $orderKerjaId = $cekData->id;
            $this->anu($orderKerjaId);
        }

        $deadline = '';
        switch ($request->deadline_costum) {
            case 'besok':
                $deadline = date('Y-m-d', strtotime('+1 day'));
                break;
            case 'lusa':
                $deadline = date('Y-m-d', strtotime('+2 day'));
                break;
            case 'pilih':
                $deadline = $request->pilih_deadline_costum;
                break;
            default:
                $deadline =  date('Y-m-d');
                break;
        }
        $subOrderKerjaBaru = new OrderKerjaSub;
        $subOrderKerjaBaru->order_kerja_id = $orderKerjaId;
        $subOrderKerjaBaru->produk_id = 5;
        $subOrderKerjaBaru->qty = $request->qty;
        $subOrderKerjaBaru->deadline = $deadline;
        $subOrderKerjaBaru->harga = $request->harga;
        $subOrderKerjaBaru->total = $request->total;
        $subOrderKerjaBaru->diskon = $request->diskon;
        $subOrderKerjaBaru->keterangan = 'Nama Produk : ' . $request->nama_produk;
        $subOrderKerjaBaru->keterangan .= '<br />Keterangan : ' . nl2br($request->keterangan);
        $subOrderKerjaBaru->save();


        return redirect()->route('order.show', $orderKerjaId);
    }

    function piut($id){
        $piutang = Piutang::create([
            'order_kerja_id'        => $id,
            'sudah_bayar'           => 0,
            'status_pembayaran'     => 'Utang'
        ]);
    }

    function anu($id){
        Piutang::where('order_kerja_id', $id)->update([
            'status_pembayaran' => 'Utang'
        ]);
    }
    function una($id){
        Piutang::where('order_kerja_id', $id)->update([
            'status_pembayaran' => 'Lunas'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderKerja  $orderKerja
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = OrderKerjaSub::where('order_kerja_id', $id)->get();
        $order = OrderKerja::find($id);
        $totalan = OrderKerjaSub::select(DB::raw('SUM(total) AS total'))->where('order_kerja_id', '=', $id)->first();
        $editors = Editor::all();
        return view('transaksi.order.order_detail', compact(['data', 'id', 'totalan', 'order','editors']));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderKerja  $orderKerja
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = OrderKerja::findOrFail($id);
        $data = OrderKerjaSub::where('order_kerja_id', $id)->get();
        $hargas = Harga::all();
        $barangs = Barang::all();
        $printers = Printer::all();
        $members = Pelanggan::all();
        $totalan = OrderKerjaSub::select(DB::raw('SUM(total) AS total'))->where('order_kerja_id', '=', $id)->first();
        $editors = Editor::all();
        return view('transaksi.order.order_edit', compact(['data', 'hargas', 'barangs', 'printers', 'members', 'order', 'totalan','editors']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderKerja  $orderKerja
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderKerja $id)
    {
        $subOrderKerjaBaru = new OrderKerjaSub;
        $subOrderKerjaBaru->order_kerja_id = $id;
        $subOrderKerjaBaru->pekerjaan_id = $request->pekerjaan;
        $subOrderKerjaBaru->finishing = $request->finishing;
        $subOrderKerjaBaru->file = $request->namafile;
        $subOrderKerjaBaru->diskon = $request->diskon;
        $subOrderKerjaBaru->qty = $request->qty;
        $subOrderKerjaBaru->panjang = $request->panjang;
        $subOrderKerjaBaru->lebar = $request->lebar;
        $dataHarga = Harga::where('pekerjaan_id', '=', $request->pekerjaan)->first();
        $subOrderKerjaBaru->harga = $dataHarga->harga_jual;
        $subOrderKerjaBaru->save();

        return redirect()->route('order.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderKerja  $orderKerja
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderKerja $orderKerja, $id)
    {
        OrderKerja::destroy($id);
        return redirect()->route('order.index')->with('alert-success', 'Data Order Kerja Telah Dihapus!');
    }

    public function destroySubKerja($id)
    {
        $dat = OrderKerjaSub::find($id);
        $idi = $dat->orderKerja->id;
        $da = OrderKerja::find($idi);
        $data = OrderKerjaSub::where('id', $id)->delete();
        if ($da->piutang->sudah_bayar >= $da->jumhar) $this->una($da->id);
        else $this->anu($da->id);
        $cek = OrderKerjaSub::where('order_kerja_id', $idi)->first();
        if (is_null($cek)) {
            $hpus = OrderKerja::find($idi)->delete();
            return redirect()->route('order.index')->with('alert-success', 'Data Order Kerja Telah Dihapus!');
        }else{
            return redirect()->route('order.show', $da->id);
        }
    }

}
