<?php

namespace App\Http\Controllers;

use App\Models\Harga;
use App\Models\Pelanggan;
use App\Models\HargaMerchant;

use Illuminate\Http\Request;

class HargaMerchantController extends Controller
{   

    public function getData( $barang, $pelanggan, $qty){
        if ( is_null($pelanggan) || is_null($barang) || is_null($qty) ) {
            $diskon = '-';
            $total = '-';
            $harga = '-';
        } else {
            $diskon = '-';
            $total = '-';
            $harga = '-';
            $idPelanggan = Pelanggan::findOrFail($pelanggan);
            $harga = Harga::where('produk_id', '=', '3')->where('member_id', '=', $idPelanggan->member_id)->first();

            if ( is_null($harga) ) {
                $diskon = '-';
                $total = '-';
                $harga = '-';

            } else { 
                $data = HargaMerchant::where('harga_id', $harga->id)
                                
                                ->where('barang_id', '=', $barang)
                                
                                ->where('range_min', '<=', $qty)
                                ->where('range_max', '>=', $qty)
                                ->first();

                if ( is_null($data) ) {
                    $diskon = '-';
                    $total = '-';
                    $harga = '-';
                } else {
                    $diskon = $data->disc;

                   

                    if( ( $qty <= $data->range_max ) && ( $qty >= $data->range_min ) ) {
                        $total = (($qty * $data->harga_jual) - ( ($qty * $data->harga_jual) * ($data->disc / 100) ));

                    } else { 
                        $total = 'Tidak set';
                    }

                    $harga = $data->harga_jual;


                }

              

            }

            $arr = array('diskon' =>$diskon ,'total'=>ceil($total),'harga'=>$harga );
            return $arr;


          
        }
    }

    public function getDiskon($printer, $barang, $laminating, $kaki, $produk, $pelanggan, $qty)
    {
        if ( is_null($pelanggan) || is_null($printer) || is_null($barang) || is_null($produk) || is_null($laminating) || is_null($kaki) || is_null($qty) ) {
            return '-';
        } else {
            $idPelanggan = Pelanggan::findOrFail($pelanggan);
            $harga = Harga::where('produk_id', '=', '3')->where('member_id', '=', $idPelanggan->member_id)->first();

            if ( is_null($harga) ) {
                return '-';
            } else { 
                $data = HargaMerchant::where('harga_id', $harga->id)
                                ->where('printer_id', '=', $printer)
                                ->where('barang_id', '=', $barang)
                                ->where('laminating', '=', $laminating)
                                ->where('kaki', '=', $kaki)
                                ->where('range_min', '<=', $qty)
                                ->where('range_max', '>=', $qty)
                                ->first();

                if ( is_null($data) ) {
                    return '-';
                } else {
                    return $data->disc;
                }
            }
        }
    }
    public function getTotal($printer, $barang, $laminating, $kaki, $produk, $pelanggan, $qty)
    {
        // return $printer . $barang . $laminating . $kaki . $produk . $pelanggan . $qty;
        if ( is_null($pelanggan) || is_null($printer) || is_null($barang) || is_null($produk) || is_null($laminating) || is_null($kaki) || is_null($qty) ) {
            return '-';
        } else {
            $idPelanggan = Pelanggan::findOrFail($pelanggan);
            $harga = Harga::where('produk_id', '=', '3')->where('member_id', '=', $idPelanggan->member_id)->first();

            if ( is_null($harga) ) {
                return '-';
            } else { 
                $data = HargaMerchant::where('harga_id', $harga->id)
                                ->where('printer_id', '=', $printer)
                                ->where('barang_id', '=', $barang)
                                ->where('laminating', '=', $laminating)
                                ->where('kaki', '=', $kaki)
                                ->where('range_min', '<=', $qty)
                                ->where('range_max', '>=', $qty)
                                ->first();

                if ( is_null($data) ) {
                    return '-';
                } else {
                    if( ( $qty <= $data->range_max ) && ( $qty >= $data->range_min ) ) {
                        return ($qty * $data->harga_jual) - ( ($qty * $data->harga_jual) * ($data->disc / 100) );

                    } else { 
                        return 'Tidak set';
                    }
                }
            }
        }
    }
    public function getHarga($printer, $barang, $laminating, $kaki, $produk, $pelanggan, $qty)
    {
        if ( is_null($pelanggan) || is_null($printer) || is_null($barang) || is_null($produk) || is_null($laminating) || is_null($kaki) || is_null($qty) ) {
            return '-';
        } else {
            $idPelanggan = Pelanggan::findOrFail($pelanggan);
            $harga = Harga::where('produk_id', '=', '3')->where('member_id', '=', $idPelanggan->member_id)->first();

            if ( is_null($harga) ) {
                return '-';
            } else { 
                $data = HargaMerchant::where('harga_id', $harga->id)
                                ->where('printer_id', '=', $printer)
                                ->where('barang_id', '=', $barang)
                                ->where('laminating', '=', $laminating)
                                ->where('kaki', '=', $kaki)
                                ->where('range_min', '<=', $qty)
                                ->where('range_max', '>=', $qty)
                                ->first();

                if ( is_null($data) ) {
                    return '-';
                } else {
                    return $data->harga_jual;
                }
            }
        }
    }
}
