<?php

namespace App\Http\Controllers;

use App\Models\Editor;
use Illuminate\Http\Request;

class EditorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Editor::all();
        return view('editor', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('editor_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Editor();
        $data->nama = $request->nama;
        $data->jk = $request->jk;
        $data->alamat = $request->alamat;
        $data->no_hp = $request->no_hp;
        $data->save();
        return redirect()->route('editor.index')->with('alert-success', 'Data Berhasil Ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Editor::where('id', $id)->get();
        return view('editor_edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Editor::where('id', $id)->first();
        $data->nama = $request->nama;
        $data->jk = $request->jk;
        $data->alamat = $request->alamat;
        $data->no_hp = $request->no_hp;
        $data->save();
        return redirect()->route('editor.index')->with('alert-success', 'Data Berhasil Diedit !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Editor::where('id', $id)->first();
        $data->delete();
        return redirect()->route('editor.index')->with('alert-success', 'Data Berhasil Dihapus !');
    }
}