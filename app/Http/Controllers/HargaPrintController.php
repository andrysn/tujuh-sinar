<?php

namespace App\Http\Controllers;

use App\Models\Harga;
use App\Models\HargaPrint;
use App\Models\Pelanggan;
use Illuminate\Http\Request;

class HargaPrintController extends Controller
{
    
     public function getData( $barang, $pelanggan, $qty,$ukuran,$editor,$tipe_print,$kaki){
        if ( is_null($pelanggan) || is_null($barang) || is_null($qty) ) {
            $diskon = '-';
            $total = '-';
            $harga = '-';
        } else {
            $diskon = '-';
            $total = '-';
            $harga = '-';
            $idPelanggan = Pelanggan::findOrFail($pelanggan);
            $harga = Harga::where('produk_id', '=', '4')->where('member_id', '=', $idPelanggan->member_id)->first();

            if ( is_null($harga) ) {
                $diskon = '-';
                $total = '-';
                $harga = '-';

            } else { 
                $data = HargaPrint::where('harga_id', $harga->id)
                                // ->where('editor_id', '=', $editor)
                                ->where('barang_id', '=', $barang)
                                ->where('kaki', '=', $kaki)
                                ->where('tipe_print', '=', $tipe_print)
                                ->where('range_min', '<=', $qty)
                                ->where('range_max', '>=', $qty)
                                ->first();

                if ( is_null($data) ) {
                    $diskon = '-';
                    $total = '-';
                    $harga = '-';
                } else {
                    $diskon = $data->disc;

                   

                    if( ( $qty <= $data->range_max ) && ( $qty >= $data->range_min ) ) {
                        $total = (($qty * $data->harga_jual) - ( ($qty * $data->harga_jual) * ($data->disc / 100) )) * $ukuran;

                    } else { 
                        $total = 'Tidak set';
                    }

                    $harga = $data->harga_jual;


                }

              

            }

            $arr = array('diskon' =>$diskon ,'total'=>ceil($total),'harga'=>$harga );
            return $arr;


          
        }
    }

    public function getDiskon($printer, $barang, $laminating, $sisi_laminating, $sisi, $cutting, $produk, $pelanggan, $qty)
    {
        if ( is_null($pelanggan) || is_null($printer) || is_null($barang) || is_null($produk) || is_null($laminating) || is_null($sisi_laminating) || is_null($sisi) || is_null($cutting) || is_null($qty)) {
            return '-';
        } else {
            $idPelanggan = Pelanggan::findOrFail($pelanggan);
            $harga = Harga::where('produk_id', '=', '4')->where('member_id', '=', $idPelanggan->member_id)->first();

            if ( is_null($harga) ) {
                return '-';
            } else { 
                $data = HargaPrint::where('harga_id', $harga->id)
                                ->where('printer_id', '=', $printer)
                                ->where('barang_id', '=', $barang)
                                ->where('laminating', '=', $laminating)
                                ->where('sisi_laminating', '=', $sisi_laminating)
                                ->where('sisi', '=', $sisi)
                                ->where('cutting', '=', $cutting)
                                ->where('range_min', '<=', $qty)
                                ->where('range_max', '>=', $qty)
                                ->first();

                if ( is_null($data) ) {
                    return '-';
                } else {
                    return $data->disc;
                }
            }
        }
    }
    public function getTotal($printer, $barang, $laminating, $sisi_laminating, $sisi, $cutting, $produk, $pelanggan, $qty)
    {
        if ( is_null($pelanggan) || is_null($printer) || is_null($barang) || is_null($produk) || is_null($laminating) || is_null($sisi_laminating) || is_null($sisi) || is_null($cutting) || is_null($qty)) {
            return '-';
        } else {
            $idPelanggan = Pelanggan::findOrFail($pelanggan);
            $harga = Harga::where('produk_id', '=', '4')->where('member_id', '=', $idPelanggan->member_id)->first();

            if ( is_null($harga) ) {
                return '-';
            } else { 
                $data = HargaPrint::where('harga_id', $harga->id)
                                ->where('printer_id', '=', $printer)
                                ->where('barang_id', '=', $barang)
                                ->where('laminating', '=', $laminating)
                                ->where('sisi_laminating', '=', $sisi_laminating)
                                ->where('sisi', '=', $sisi)
                                ->where('cutting', '=', $cutting)
                                ->where('range_min', '<=', $qty)
                                ->where('range_max', '>=', $qty)
                                ->first();

                if ( is_null($data) ) {
                    return '-';
                } else {
                    if( ( $qty <= $data->range_max ) && ( $qty >= $data->range_min ) ) {
                        return ($qty * $data->harga_jual) - ( ($qty * $data->harga_jual) * ($data->disc / 100) );

                    } else { 
                        return 'Tidak set';
                    }
                }
            }
        }
    }
    public function getHarga($printer, $barang, $laminating, $sisi_laminating, $sisi, $cutting, $produk, $pelanggan, $qty)
    {
        if ( is_null($pelanggan) || is_null($printer) || is_null($barang) || is_null($produk) || is_null($laminating) || is_null($sisi_laminating) || is_null($sisi) || is_null($cutting) || is_null($qty)) {
            return '-';
        } else {
            $idPelanggan = Pelanggan::findOrFail($pelanggan);
            $harga = Harga::where('produk_id', '=', '4')->where('member_id', '=', $idPelanggan->member_id)->first();

            if ( is_null($harga) ) {
                return '-';
            } else { 
                $data = HargaPrint::where('harga_id', $harga->id)
                                ->where('printer_id', '=', $printer)
                                ->where('barang_id', '=', $barang)
                                ->where('laminating', '=', $laminating)
                                ->where('sisi_laminating', '=', $sisi_laminating)
                                ->where('sisi', '=', $sisi)
                                ->where('cutting', '=', $cutting)
                                ->where('range_min', '<=', $qty)
                                ->where('range_max', '>=', $qty)
                                ->first();

                if ( is_null($data) ) {
                    return '-';
                } else {
                    return $data->harga_jual;
                }
            }
        }
    }
}
