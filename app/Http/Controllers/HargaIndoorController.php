<?php

namespace App\Http\Controllers;

use App\Models\Harga;
use App\Models\Pelanggan;
use App\Models\HargaIndoor;

use Illuminate\Http\Request;

class HargaIndoorController extends Controller
{   

        public function getData( $barang, $editor, $pelanggan, $qty,$p,$l,$tipe_print){
        if ( is_null($pelanggan) || is_null($editor) || is_null($barang) || is_null($qty) || is_null($tipe_print)) {
            return '-';
        } else {
            $diskon = '-';
            $total = '-';
            $harga = '-';
            $tp = 0;
            // if($tipe_print == 2){
            //     $tp = 15000;
            // }
            $idPelanggan = Pelanggan::findOrFail($pelanggan);
            $harga = Harga::where('produk_id', '=', '2')->where('member_id', '=', $idPelanggan->member_id)->first();

            if ( is_null($harga) ) {
                $diskon = '-';
                $total = '-';
                $harga = '-';

            } else { 
                $data = HargaIndoor::where('harga_id', $harga->id)
                                ->where('editor_id', '=', $editor)
                                ->where('barang_id', '=', $barang)
                                ->where('tipe_print', '=', $tipe_print)
                                ->where('range_min', '<=', $qty)
                                ->where('range_max', '>=', $qty)
                                ->first();

                if ( is_null($data) ) {
                    $diskon = '-';
                    $total = '-';
                    $harga = '-';
                } else {
                    $diskon = $data->disc;

                    $luas = (int)$p*(int)$l;

                    if( ( $qty <= $data->range_max ) && ( $qty >= $data->range_min ) ) {
                        $total = ((($qty * $data->harga_jual) - ( ($qty * $data->harga_jual) * ($data->disc / 100) )) * $p * $l);

                    } else { 
                        $total = 'Tidak set';
                    }

                    $harga = $data->harga_jual;

                    
                }

              

            }

            $arr = array('diskon' =>$diskon ,'total'=>ceil($total),'harga'=>$harga );
            return $arr;
          
        }
    }

//     public function getDiskon($printer, $barang, $sisi, $ukuran, $produk, $pelanggan, $qty)
//     {
//         if ( is_null($pelanggan) || is_null($printer) || is_null($barang) || is_null($ukuran) || is_null($sisi) || is_null($produk) || is_null($qty) ) {
//             return '-';
//         } else {
            
//             $idPelanggan = Pelanggan::findOrFail($pelanggan);
//             $harga = Harga::where('produk_id', '=', '2')->where('member_id', '=', $idPelanggan->member_id)->first();

//             if ( is_null($harga) ) {
//                 return '-';
//             } else { 
//                 $data = HargaIndoor::where('harga_id', $harga->id)
//                                 ->where('printer_id', '=', $printer)
//                                 ->where('barang_id', '=', $barang)
//                                 ->where('ukuran', '=', $ukuran)
//                                 ->where('sisi', '=', $sisi)
//                                 ->where('range_min', '<=', $qty)
//                                 ->where('range_max', '>=', $qty)
//                                 ->first();

//                 if ( is_null($data) ) {
//                     return '-';
//                 } else {
//                     return $data->disc;
//                 }
//             }
//         }
//     }
//     public function getTotal($printer, $barang, $sisi, $ukuran, $produk, $pelanggan, $qty)
//     {
//         if ( is_null($pelanggan) || is_null($printer) || is_null($barang) || is_null($ukuran) || is_null($sisi) || is_null($produk) || is_null($qty) ) {
//             return '-';
//         } else {
//             $idPelanggan = Pelanggan::findOrFail($pelanggan);
//             $harga = Harga::where('produk_id', '=', '2')->where('member_id', '=', $idPelanggan->member_id)->first();

//             if ( is_null($harga) ) {
//                 return '-';
//             } else { 
//                 $data = HargaIndoor::where('harga_id', $harga->id)
//                                 ->where('printer_id', '=', $printer)
//                                 ->where('barang_id', '=', $barang)
//                                 ->where('ukuran', '=', $ukuran)
//                                 ->where('sisi', '=', $sisi)
//                                 ->where('range_min', '<=', $qty)
//                                 ->where('range_max', '>=', $qty)
//                                 ->first();

//                 if ( is_null($data) ) {
//                     return '-';
//                 } else {
//                     if( ( $qty <= $data->range_max ) && ( $qty >= $data->range_min ) ) {
//                         return ($qty * $data->harga_jual) - ( ($qty * $data->harga_jual) * ($data->disc / 100) );

//                     } else { 
//                         return 'Tidak set';
//                     }
//                 }
//             }
//         }
//     }
//     public function getHarga($printer, $barang, $sisi, $ukuran, $produk, $pelanggan, $qty)
//     {
//         if ( is_null($pelanggan) || is_null($printer) || is_null($barang) || is_null($ukuran) || is_null($sisi) || is_null($produk) || is_null($qty) ) {
//             return '-';
//         } else {
            
//             $idPelanggan = Pelanggan::findOrFail($pelanggan);
//             $harga = Harga::where('produk_id', '=', '2')->where('member_id', '=', $idPelanggan->member_id)->first();

//             if ( is_null($harga) ) {
//                 return '-';
//             } else { 
//                 $data = HargaIndoor::where('harga_id', $harga->id)
//                                 ->where('printer_id', '=', $printer)
//                                 ->where('barang_id', '=', $barang)
//                                 ->where('ukuran', '=', $ukuran)
//                                 ->where('sisi', '=', $sisi)
//                                 ->where('range_min', '<=', $qty)
//                                 ->where('range_max', '>=', $qty)
//                                 ->first();

//                 if ( is_null($data) ) {
//                     return '-';
//                 } else {
//                     return $data->harga_jual;
//                 }
//             }
//         }
//     }
}
