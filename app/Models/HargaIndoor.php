<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HargaIndoor extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public static $x     = '1';
    public static $rolbn = '2';
    public static $ybnr  = '3';
    
    public function getTripodAttribute()
    {
        $label = [
            HargaIndoor::$x     => 'Print Only',
            HargaIndoor::$rolbn => 'Cut Only',
            HargaIndoor::$ybnr  => 'Print + Cut'
        ];
        return $label[$this->tipe_print];
    }

    public function Harga()
    {
    	return $this->belongsTo(Harga::class);
    }
    public function Printer()
    {
    	return $this->belongsTo(Printer::class);
    }
    public function Barang()
    {
    	return $this->belongsTo(Barang::class);
    }
    public function Finishing()
    {
        return $this->belongsTo(Editor::class,'editor_id');
    }

    public function getKakinyaAttribute(){
        $kaki = '';
        switch ($this->kaki) {
            case '1':
                $kaki = "X-Banner";
                break;
            case '2':
                $kaki = "Roll Banner";
                break;
            case '3':
                $kaki = "Y Banner";
                break;
            case '3':
                $kaki = "Rollup";
                break;
            case '4':
                $kaki = "Toneup";
                break;
            default:
                $kaki = "";
                break;
        }
        return $kaki;
    }
}
