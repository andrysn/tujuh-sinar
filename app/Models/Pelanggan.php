<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pelanggan extends Model
{
    protected $guarded = [];

    public function Member()
    {
    	return $this->belongsTo(Member::class);
    }
    public function OrderKerja()
    {
        return $this->hasMany(OrderKerja::class);
    }
}
