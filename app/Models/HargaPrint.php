<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HargaPrint extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    
    public static $x     = '1';
    public static $rolbn = '2';
    public static $ybnr  = '3';
    public static $rolup = '4';
    public static $tonup = '5';
    
    public function getTripodAttribute()
    {
        $label = [
            HargaPrint::$x     => 'X Banner',
            HargaPrint::$rolbn => 'Roll Banner',
            HargaPrint::$ybnr  => 'Y Banner',
            HargaPrint::$rolup => 'Rollup Banner',
            HargaPrint::$tonup => 'Toneup Banner'
        ];
        return $label[$this->data];
    }

    public function Harga()
    {
    	return $this->belongsTo(Harga::class);
    }
    public function Editor()
    {
        return $this->belongsTo(Editor::class);
    }

    public function Finishing()
    {
        return $this->belongsTo(Editor::class,'editor_id');
    }
    public function Barang()
    {
    	return $this->belongsTo(Barang::class);
    }

    public function getTipeAttribute(){
        $data = '';
        switch ($this->tipe_print) {
            case '1':
                $data = "1 Sisi";
                break;
            case '2':
                $data = "2 Sisi";
                break;
           
            default:
                $data = "";
                break;
        }
        return $data;
    }

    public function getKakinyaAttribute(){
        $kaki = '';
        switch ($this->kaki) {
            case '1':
                $kaki = "X-Banner";
                break;
            case '2':
                $kaki = "Roll Banner";
                break;
            case '3':
                $kaki = "Y Banner";
                break;
            case '3':
                $kaki = "Rollup";
                break;
            case '4':
                $kaki = "Toneup";
                break;
            default:
                $kaki = "";
                break;
        }
        return $kaki;
    }
}
