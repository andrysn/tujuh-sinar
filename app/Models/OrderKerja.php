<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderKerja extends Model
{
    protected $guarded = [];

    public function OrderKerjaSub()
    {
    	return $this->hasMany(OrderKerjaSub::class, 'order_kerja_id', 'id');
    }
    public function Piutang()
    {
        return $this->hasOne(Piutang::class);
    }
    public function Pelanggan()
    {
    	return $this->belongsTo(Pelanggan::class);
    }
    public function getJumharAttribute(){
        $jum = 0;
        foreach ($this->orderkerjasub as $i) {
            $jum += $i->harga * $i->qty;
        }
        return $jum;
    }
    public function getTanggalOrdAttribute($tanggal_ord)
    {   
        $tgl_deadline = $this->tanggal;
        \Carbon\Carbon::setLocale('id');

        switch(\Carbon\Carbon::parse($tanggal_ord)->format('l')) {
            case 'Monday' :
                $hari = 'Senin';
                break;
            case 'Tuesday' :
                $hari = 'Selasa';
                break;
            case 'Wednesday' :
                $hari = 'Rabu';
                break;
            case 'Thursday' :
                $hari = 'Kamis';
                break;
            case 'Friday' :
                $hari = 'Jumat';
                break;
            case 'Saturday' :
                $hari = 'Sabtu';
                break;
            default:
                $hari = 'Minggu';
        }

        switch(\Carbon\Carbon::parse($tanggal_ord)->format('F')) {
            case 'January' :
                $bulan = 'Jan';
                break;
            case 'February' :
                $bulan = 'Feb';
                break;
            case 'March' :
                $bulan = 'Maret';
                break;
            case 'April' :
                $bulan = 'April';
                break;
            case 'May' :
                $bulan = 'Mei';
                break;
            case 'June' :
                $bulan = 'Juni';
                break;
            case 'July' :
                $bulan = 'Juli';
                break;
            case 'August' :
                $bulan = 'Agst';
                break;
            case 'September' :
                $bulan = 'Sept';
                break;
            case 'October' :
                $bulan = 'Okt';
                break;
            case 'November' :
                $bulan = 'Nov';
                break;
            default:
                $bulan = 'Dec';
        }


        return $hari . ', ' . \Carbon\Carbon::parse($tanggal_ord)
            ->format('d-') . $bulan . \Carbon\Carbon::parse($tanggal_ord)
            ->format('-Y');
    }
}