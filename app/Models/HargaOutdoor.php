<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HargaOutdoor extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function Harga()
    {
        return $this->belongsTo(Harga::class);
    }
    public function Editor()
    {
        return $this->belongsTo(Editor::class);
    }

    public function Finishing()
    {
        return $this->belongsTo(Editor::class,'editor_id');
    }
    public function getNamaEditorAttribute()
    {
        if ( $this->editor ) {
            return $this->editor->nama_finishing;
        }
    }
    public function Barang()
    {
    	return $this->belongsTo(Barang::class);
    }
}