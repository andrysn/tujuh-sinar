<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Harga extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function Member()
    {
    	return $this->belongsTo(Member::class);
    }
    public function HargaOutdoor()
    {
    	return $this->hasMany('HargaOutdoor');
    }
    public function HargaIndoor()
    {
    	return $this->hasMany('HargaIndoor');
    }
    public function HargaPrint()
    {
    	return $this->hasMany('HargaPrint');
    }
    public function HargaMerchant()
    {
    	return $this->hasMany('HargaMerchant');
    }
    public function HargaCostum()
    {
    	return $this->hasMany('HargaCostum');
    }
}